docker rm -f condominio-db
docker build -t condominio-db .
docker run -it --name condominio-db -e POSTGRES_PASSWORD=root -v ~/docker-volumes/condominio/postgres-9.5/:/var/lib/postgresql/data:rw -p 5432:5432 -d condominio-db
