package br.com.tagitta.condominio.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import br.com.syncode.arquitetura.spring.ContextoSpring;

@Component
public class SpringContext implements ApplicationContextAware {

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextoSpring.acrescentaContexto("condominio", applicationContext);
	}
}