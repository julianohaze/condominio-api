package br.com.tagitta.condominio.model.to;

public class AreaDeReservaDto {
  private String condominioId;
  private String descricao;

  public String getCondominioId() {
    return condominioId;
  }

  public void setCondominioId(String condominioId) {
    this.condominioId = condominioId;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }
}
