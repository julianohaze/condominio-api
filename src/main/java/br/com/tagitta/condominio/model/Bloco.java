package br.com.tagitta.condominio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.arquitetura.util.UtilString;

@Table
@Entity
public class Bloco extends Entidade<String> {
  private String descricao;
  @JsonIgnore
  private Condominio condominio;

  private Bloco() {}

  public Bloco(String descricao) {
    this();
    this.descricao = descricao;
  }

  @Id
  @GeneratedValue(generator = "UUIDGenerator")
  @GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
  @Column(name = "bloco_id", length = 32)
  @Override
  public String getId() {
    return super.getId();
  }

  @Column(nullable = false, length = 100)
  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  @ManyToOne
  @JoinColumn(name = "condominio_id")
  public Condominio getCondominio() {
    return condominio;
  }

  public void setCondominio(Condominio condominio) {
    this.condominio = condominio;
  }

  @Override
  public void persiste() {
    if (UtilString.isVazio(this.descricao)) {
      throw new IllegalArgumentException("A descrição do bloco deve ser informada.");
    }

    super.persiste();
  }
}
