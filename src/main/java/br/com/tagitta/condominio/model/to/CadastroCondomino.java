package br.com.tagitta.condominio.model.to;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.integracao.DAO;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Usuario;

public class CadastroCondomino {
  private String nome;
  private String email;
  private String senha;
  private String condominioId;
  private String unidade;
  private String blocoId;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCondominioId() {
    return condominioId;
  }

  public void setCondominioId(String condominioId) {
    this.condominioId = condominioId;
  }

  public String getUnidade() {
    return unidade;
  }

  public void setUnidade(String unidade) {
    this.unidade = unidade;
  }

  public String getBlocoId() {
    return blocoId;
  }

  public void setBlocoId(String blocoId) {
    this.blocoId = blocoId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  public Condomino toCondomino() {
    DAO dao = Aplicacao.get().getDAO();

    Condominio condominio = dao.getObjeto(Condominio.class, condominioId);

    Usuario usuario = Usuario.getPorEmail(email);
    if (usuario == null) {
      usuario = new Usuario();
      usuario.setEmail(email);
      usuario.setNome(nome);
      usuario.setSenha(senha);
      usuario.persiste();
    }

    Bloco bloco = null;
    if (blocoId != null) {
      bloco = dao.getObjeto(Bloco.class, blocoId);
    }
    return condominio.acrescentaCondomino(usuario, unidade, bloco);
  }
}
