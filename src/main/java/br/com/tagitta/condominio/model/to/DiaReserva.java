package br.com.tagitta.condominio.model.to;

import java.time.LocalDateTime;

public class DiaReserva {
	private String reservaId;
	private LocalDateTime data;

	public String getReservaId() {
		return reservaId;
	}

	public void setReservaId(String reservaId) {
		this.reservaId = reservaId;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}
}
