package br.com.tagitta.condominio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AreaDeReserva extends Entidade<String> implements Comparable<AreaDeReserva> {
	private String descricao;
	private Condominio condominio;

	private AreaDeReserva() {
	}

	public AreaDeReserva(String descricao, Condominio condominio) {
		this();
		this.descricao = descricao;
		this.condominio = condominio;
	}

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "areadereserva_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Column
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "condominio_id", nullable = false)
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@Override
	public int compareTo(AreaDeReserva o) {
		int resultado = Aplicacao.get().getOrdenador().compare(getDescricao(), o.getDescricao());
		if (resultado == 0) {
			resultado = getId().compareTo(o.getId());
		}
		return resultado;
	}
}
