package br.com.tagitta.condominio.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.arquitetura.entidade.Entidade;

@Entity
public class UsuarioNoCondominio extends Entidade<String> {
	private Condominio condominio;
	private Usuario usuario;
	private Set<PapelNoCondominio> papeis;

	private UsuarioNoCondominio() {
	}

	public UsuarioNoCondominio(Condominio condominio, Usuario usuario) {
		this();
		this.condominio = condominio;
		this.usuario = usuario;
	}

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "usuarionocondominio_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@ManyToOne
	@JoinColumn(name = "condominio_id", nullable = false)
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@OneToMany(mappedBy = "usuarioNoCondominio")
	public Set<PapelNoCondominio> getPapeis() {
		if (papeis == null) {
			papeis = new HashSet<>();
		}
		return papeis;
	}

	public void setPapeis(Set<PapelNoCondominio> papeis) {
		this.papeis = papeis;
	}

	public void acrescentaPapel(PapelNoCondominio papel) {
		papel.setUsuarioNoCondominio(this);
		papel.persiste();
		getPapeis().add(papel);

		getUsuario().acrescentaCondominio(this);
	}
}