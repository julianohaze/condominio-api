package br.com.tagitta.condominio.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.util.Assert;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.tagitta.condominio.exception.UsuarioJaPossuiPapelNoCondominioException;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;
import br.com.tagitta.condominio.repository.CondominiosRepository;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Condominio extends Entidade<String> {
  private String nome;
  private String endereco;
  private Sindico sindico;
  private Integer mudancasPorBloco = 1;
  private List<Bloco> blocos;

  public boolean possuiBlocos() {
    return getBlocos().size() > 0;
  }

  public Sindico acrescentaSindico(Usuario usuario, LocalDate entrada) {
    if (usuario.isSindicoDo(this)) {
      throw new UsuarioJaPossuiPapelNoCondominioException(usuario, this, TipoDePapelNoCondominio.SINDICO);
    }

    Assert.notNull(entrada, "A data de entrada do síndico deve ser informada!");

    Sindico sindico = new Sindico();
    sindico.setEntrada(entrada);

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio(usuario);
    usuarioNoCondominio.acrescentaPapel(sindico);

    Condominio condominio = usuarioNoCondominio.getCondominio();
    condominio.setSindico(sindico);
    condominio.persiste();

    criaChatComTodosCondominos(usuario);

    return sindico;
  }

  private void criaChatComTodosCondominos(Usuario usuario) {
    Collection<Condomino> condominos = getCondominos();
    condominos.forEach(condomino -> {
      condomino.getUsuario().criaChatCom(sindico.getUsuario(), this);
    });
  }

  @Transient
  private Collection<Condomino> getCondominos() {
    CondominiosRepository condominios = Aplicacao.get().getRepositorio(CondominiosRepository.class);
    return condominios.getCondominos(this);
  }

  public void acrescentaAdministrador(Usuario usuario) {
    if (usuario.isAdministradorDo(this)) {
      throw new UsuarioJaPossuiPapelNoCondominioException(usuario, this, TipoDePapelNoCondominio.ADMINISTRADOR);
    }

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio(usuario);
    usuarioNoCondominio.acrescentaPapel(new Administrador());
  }

  public Condomino acrescentaCondomino(Usuario usuario, String unidade) {
    return acrescentaCondomino(usuario, unidade, null);
  }

  public Condomino acrescentaCondomino(Usuario usuario, String unidade, Bloco bloco) {
    if (usuario.isCondominoDo(this)) {
      throw new UsuarioJaPossuiPapelNoCondominioException(usuario, this, TipoDePapelNoCondominio.CONDOMINO);
    }

    Assert.notNull(unidade, "A unidade do condômino deve ser informada!");

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio(usuario);
    Condomino condomino = new Condomino(unidade, bloco);
    usuarioNoCondominio.acrescentaPapel(condomino);

    criaChatComOSindico(condomino);

    return condomino;
  }

  private void criaChatComOSindico(Condomino condomino) {
    Usuario sindico = this.getSindico().getUsuario();

    Chat.cria(this, condomino.getUsuario(), sindico);
  }

  private UsuarioNoCondominio getUsuarioNoCondominio(Usuario usuario) {
    UsuarioNoCondominio usuarioNoCondominio = repository().getUsuarioNoCondominio(usuario, this);
    if (usuarioNoCondominio == null) {
      usuarioNoCondominio = new UsuarioNoCondominio(this, usuario);
      usuarioNoCondominio.persiste();
    }
    return usuarioNoCondominio;
  }

  @Id
  @GeneratedValue(generator = "UUIDGenerator")
  @GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
  @Column(name = "condominio_id", length = 32)
  @Override
  public String getId() {
    return super.getId();
  }

  @Column(nullable = false, length = 200)
  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  @Column(nullable = true, length = 300)
  public String getEndereco() {
    return endereco;
  }

  public void setEndereco(String endereco) {
    this.endereco = endereco;
  }

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "sindico_id")
  public Sindico getSindico() {
    return sindico;
  }

  public void setSindico(Sindico sindico) {
    this.sindico = sindico;
  }

  @Column
  public Integer getMudancasPorBloco() {
    return mudancasPorBloco;
  }

  public void setMudancasPorBloco(Integer mudancasPorBloco) {
    this.mudancasPorBloco = mudancasPorBloco;
  }

  @OneToMany(mappedBy = "condominio")
  public List<Bloco> getBlocos() {
    if (blocos == null) {
      blocos = new ArrayList<Bloco>();
    }
    return blocos;
  }

  public void setBlocos(List<Bloco> blocos) {
    this.blocos = blocos;
  }

  public static CondominiosRepository repository() {
    return Aplicacao.get().getRepositorio(CondominiosRepository.class);
  }

  public Bloco acrescentarBloco(String descricao) {
    Bloco bloco = new Bloco(descricao);
    return acrescentarBloco(bloco);
  }

  public Bloco acrescentarBloco(Bloco bloco) {
    bloco.setCondominio(this);
    bloco.persiste();

    getBlocos().add(bloco);

    return bloco;
  }
}
