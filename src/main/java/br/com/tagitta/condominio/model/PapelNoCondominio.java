package br.com.tagitta.condominio.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo", discriminatorType = DiscriminatorType.STRING)
public abstract class PapelNoCondominio extends Entidade<String> {
	private UsuarioNoCondominio usuarioNoCondominio;

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "papelnocondominio_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}
	
	@JsonIgnore
	@Transient
	public Usuario getUsuario() {
		return getUsuarioNoCondominio().getUsuario();
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "usuarionocondominio_id", nullable = false)
	public UsuarioNoCondominio getUsuarioNoCondominio() {
		return usuarioNoCondominio;
	}

	public void setUsuarioNoCondominio(UsuarioNoCondominio usuarioNoCondominio) {
		this.usuarioNoCondominio = usuarioNoCondominio;
	}

	@Transient
	public abstract TipoDePapelNoCondominio getTipo();
}