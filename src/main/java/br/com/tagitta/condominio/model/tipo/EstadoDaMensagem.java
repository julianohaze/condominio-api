package br.com.tagitta.condominio.model.tipo;

import br.com.syncode.arquitetura.entidade.EnumeradoPersistente;

public enum EstadoDaMensagem implements EnumeradoPersistente {
	ENVIADA("ENV", "Enviada"), ENTREGUE("ENT", "Entregue"), LIDA("LID", "Lida");

	private String id;
	private String descricao;

	private EstadoDaMensagem(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	@Override
	public String getDescricao() {
		return descricao;
	}
}