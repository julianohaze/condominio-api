package br.com.tagitta.condominio.model;

import java.time.LocalDate;

import br.com.tagitta.condominio.model.tipo.Periodo;
import br.com.tagitta.condominio.model.tipo.TipoDeAgendamentoDeMudanca;

public class AgendamentoDeMudancaBuilder {

	private AgendamentoDeMudanca agendamento;

	public AgendamentoDeMudancaBuilder(AgendamentoDeMudanca agendamento) {
		this.agendamento = agendamento;
	}
	
	public AgendamentoDeMudancaBuilder para(Condomino condomino) {
		agendamento.setCondomino(condomino);
		return this;
	}
	
	public AgendamentoDeMudancaBuilder naData(LocalDate data) {
		agendamento.setData(data);
		return this;
	}
	
	public AgendamentoDeMudancaBuilder doTipo(TipoDeAgendamentoDeMudanca tipo) {
		agendamento.setTipo(tipo);
		return this;
	}
	
	public AgendamentoDeMudancaBuilder noPeriodo(Periodo periodo) {
		agendamento.setPeriodo(periodo);
		return this;
	}
	
	
	public AgendamentoDeMudanca build() {
		return agendamento;
	}

}
