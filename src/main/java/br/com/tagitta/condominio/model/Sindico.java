package br.com.tagitta.condominio.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.integracao.hibernate.converter.LocalDateConverter;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;

@Entity
@DiscriminatorValue("sind")
public class Sindico extends PapelNoCondominio {
	private LocalDate entrada;
	private LocalDate saida;

	@Column
	@Convert(converter = LocalDateConverter.class)
	public LocalDate getEntrada() {
		return entrada;
	}

	public void setEntrada(LocalDate entrada) {
		this.entrada = entrada;
	}

	@Column
	@Convert(converter = LocalDateConverter.class)
	public LocalDate getSaida() {
		return saida;
	}

	public void setSaida(LocalDate saida) {
		this.saida = saida;
	}

	@Transient
	@Override
	public TipoDePapelNoCondominio getTipo() {
		return TipoDePapelNoCondominio.SINDICO;
	}

	@JsonIgnore
	@Transient
	public Usuario getUsuario() {
		return getUsuarioNoCondominio().getUsuario();
	}
}