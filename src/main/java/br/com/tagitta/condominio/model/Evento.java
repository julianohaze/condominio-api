package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.model.tipo.TipoDeEvento;

@Entity
public class Evento extends Entidade<String> {
	private LocalDateTime data;
	private TipoDeEvento tipo;
	private String descricao;
	private Condominio condominio;

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "evento_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.TipoDeEventoUserType")
	public TipoDeEvento getTipo() {
		return tipo;
	}

	public void setTipo(TipoDeEvento tipo) {
		this.tipo = tipo;
	}

	@Column(length = 100)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "condominio_id")
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
}