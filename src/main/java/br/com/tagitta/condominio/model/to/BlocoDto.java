package br.com.tagitta.condominio.model.to;

public class BlocoDto {
  private String descricao;
  private String condominioId;

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public String getCondominioId() {
    return condominioId;
  }

  public void setCondominioId(String condominioId) {
    this.condominioId = condominioId;
  }
}
