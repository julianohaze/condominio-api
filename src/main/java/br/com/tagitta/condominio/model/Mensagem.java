package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import base.util.UUIDGenerator;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.model.tipo.EstadoDaMensagem;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Mensagem extends Entidade<String> implements Comparable<Mensagem> {
	private LocalDateTime dataHora;
	private String texto;
	private Usuario usuario;
	private Chat chat;
	private EstadoDaMensagem estado;
	private LocalDateTime dataDeAtualizacao;

	private Mensagem() {
	}

	@Id
	@Column(name = "mensagem_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Convert(converter = LocalDateTimeConverter.class)
	@Column(nullable = false)
	public LocalDateTime getDataHora() {
		return dataHora;
	}

	protected void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}

	@Column(nullable = false)
	public String getTexto() {
		return texto;
	}

	protected void setTexto(String texto) {
		this.texto = texto;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	public Usuario getUsuario() {
		return usuario;
	}

	protected void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "chat_id")
	public Chat getChat() {
		return chat;
	}

	protected void setChat(Chat chat) {
		this.chat = chat;
	}

	@Column
	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.EstadoDaMensagemUserType")
	public EstadoDaMensagem getEstado() {
		return estado;
	}

	public void setEstado(EstadoDaMensagem estado) {
		this.estado = estado;
	}

	@Override
	public int compareTo(Mensagem o) {
		int resultado = o.getDataHora().compareTo(getDataHora());
		if (resultado == 0) {
			resultado = getId().compareTo(o.getId());
		}
		return resultado;
	}

	public static MensagemBuilder cria() {
		return new MensagemBuilder(new Mensagem());
	}

	public void confirmaRecebimento() {
		this.estado = EstadoDaMensagem.ENTREGUE;
		this.persiste();
	}

	public void confirmaLeitura() {
		this.estado = EstadoDaMensagem.LIDA;
		this.persiste();
	}
	
	@Convert(converter = LocalDateTimeConverter.class)
	@Column
	public LocalDateTime getDataDeAtualizacao() {
		return dataDeAtualizacao;
	}
	
	public void setDataDeAtualizacao(LocalDateTime dataDeAtualizacao) {
		this.dataDeAtualizacao = dataDeAtualizacao;
	}

	@Override
	public void persiste() {
		if (getId() == null) {
			setId(UUIDGenerator.generate());
		}
		this.dataDeAtualizacao = LocalDateTime.now();
		
		super.persiste();
	}
	
	public void inclui() {
		this.estado = EstadoDaMensagem.ENVIADA;
		persiste();
		
		this.chat.setUltimaMensagemRecebida(this);
		this.chat.setDataHoraDaUltimaMensagem(this.getDataHora());
		this.getChat().persiste();
	}
}