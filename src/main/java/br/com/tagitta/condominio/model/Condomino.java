package br.com.tagitta.condominio.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;

@Entity
@DiscriminatorValue("con")
public class Condomino extends PapelNoCondominio {
	private String unidade;
	private Bloco bloco;

	private Condomino() {
	}

	public Condomino(String unidade, Bloco bloco) {
		this();
		this.unidade = unidade;
		this.bloco = bloco;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	@ManyToOne
	@JoinColumn(name = "bloco_id")
	public Bloco getBloco() {
		return bloco;
	}

	public void setBloco(Bloco bloco) {
		this.bloco = bloco;
	}

	@Transient
	public String getNome() {
		return getUsuarioNoCondominio().getUsuario().getNome();
	}

	@Transient
	@Override
	public TipoDePapelNoCondominio getTipo() {
		return TipoDePapelNoCondominio.CONDOMINO;
	}

	@Transient
	public Condominio getCondominio() {
		return getUsuarioNoCondominio().getCondominio();
	}
}
