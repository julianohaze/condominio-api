package br.com.tagitta.condominio.model.to;

import br.com.tagitta.condominio.model.Usuario;

public class UsuarioDto {
	private String id;
	private String nome;
	private String email;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static UsuarioDto from(Usuario usuario) {
		UsuarioDto dto = new UsuarioDto();
		dto.setEmail(usuario.getEmail());
		dto.setId(usuario.getId());
		dto.setNome(usuario.getNome());
		return dto;
	}
}