package br.com.tagitta.condominio.model;

import java.util.ArrayList;
import java.util.List;

public class CadastroDeCondominio {
	private String nome;
	private String endereco;
	private String nomeSindico;
	private String email;
	private String senha;
	private List<String> blocos;

	public void acrescentaBloco(String bloco) {
		getBlocos().add(bloco);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNomeSindico() {
		return nomeSindico;
	}

	public void setNomeSindico(String nomeSindico) {
		this.nomeSindico = nomeSindico;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<String> getBlocos() {
		if (blocos == null) {
			blocos = new ArrayList<String>();
		}
		return blocos;
	}

	public void setBlocos(List<String> blocos) {
		this.blocos = blocos;
	}
}