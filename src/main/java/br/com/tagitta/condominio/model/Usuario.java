package br.com.tagitta.condominio.model;

import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import base.util.UUIDGenerator;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.arquitetura.util.UtilString;
import br.com.tagitta.condominio.exception.AutenticacaoException;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;
import br.com.tagitta.condominio.model.to.UsuarioAutenticacao;
import br.com.tagitta.condominio.repository.ChatRepository;
import br.com.tagitta.condominio.repository.UsuariosRepository;
import br.com.tagitta.condominio.utils.CriptografiaUtils;

@Entity
public class Usuario extends Entidade<String> {
  private String nome;
  private String email;
  private String senha;
  private String senhaCrypt;
  private String hash;
  private Set<UsuarioNoCondominio> condominiosDoUsuario;

  @Id
  @GeneratedValue(generator = "UUIDGenerator")
  @GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
  @Column(name = "usuario_id", length = 32)
  @Override
  public String getId() {
    return super.getId();
  }

  @Column(length = 150, nullable = false)
  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  @Column(length = 150, nullable = false)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @JsonIgnore
  @Column(length = 64, nullable = false)
  public String getSenhaCrypt() {
    return senhaCrypt;
  }

  private void setSenhaCrypt(String senha) {
    this.senhaCrypt = senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  @Column(length = 32, nullable = true)
  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  @Transient
  @JsonIgnore
  public List<Condominio> getCondominios() {
    List<Condominio> condominios = new ArrayList<>();
    for (UsuarioNoCondominio usuarioNoCondominio : getCondominiosDoUsuario()) {
      condominios.add(usuarioNoCondominio.getCondominio());
    }

    condominios.sort(comparing(Condominio::getNome));
    return condominios;
  }

  @OneToMany(mappedBy = "usuario")
  public Set<UsuarioNoCondominio> getCondominiosDoUsuario() {
    if (condominiosDoUsuario == null) {
      condominiosDoUsuario = new HashSet<>();
    }
    return condominiosDoUsuario;
  }

  @SuppressWarnings("unused")
  private void setCondominiosDoUsuario(Set<UsuarioNoCondominio> condominiosDoUsuario) {
    this.condominiosDoUsuario = condominiosDoUsuario;
  }

  @Transient
  public Set<PapelNoCondominio> getPapeisNoCondominio(Condominio condominio) {
    Set<PapelNoCondominio> papeis = new HashSet<>();
    for (UsuarioNoCondominio usuarioNoCondominio : getCondominiosDoUsuario()) {
      if (usuarioNoCondominio.getCondominio().equals(condominio)) {
        for (PapelNoCondominio papel : usuarioNoCondominio.getPapeis()) {
          papeis.add(papel);
        }
      }
    }
    return papeis;
  }

  @Override
  public void persiste() {
    if (getId() == null) {
      setSenhaCrypt(CriptografiaUtils.criptografa(senha));
    }
    if (UtilString.isVazio(nome))
      throw new DadosInvalidosException("O nome deve ser informado.");

    if (UtilString.isVazio(email))
      throw new DadosInvalidosException("O email deve ser informado.");

    if (!email.contains("@") || !email.contains("."))
      throw new DadosInvalidosException("Por favor, informe um e-mail válido.");

    super.persiste();
  }

  public void atualizaSenha(String novaSenha) {
    this.setSenha(novaSenha);
    setSenhaCrypt(CriptografiaUtils.criptografa(novaSenha));
    persiste();
  }

  @JsonIgnore
  @Transient
  public boolean isSenhaValida() {
    String criptografada = CriptografiaUtils.criptografa(senha);
    return this.senhaCrypt.equals(criptografada);
  }

  public String autentica(UsuarioAutenticacao usuario) {
    this.senha = usuario.getSenha();
    if (!isSenhaValida()) {
      throw new AutenticacaoException("Usuário ou senha inválidos!");
    }

    this.hash = UUIDGenerator.generate();
    persiste();
    return hash;
  }

  @Transient
  public boolean isCondominoDo(Condominio condominio) {
    return possuiPapelNoCondominio(condominio, TipoDePapelNoCondominio.CONDOMINO);
  }

  @Transient
  public boolean isAdministradorDo(Condominio condominio) {
    return possuiPapelNoCondominio(condominio, TipoDePapelNoCondominio.ADMINISTRADOR);
  }

  @Transient
  public boolean isSindicoDo(Condominio condominio) {
    return possuiPapelNoCondominio(condominio, TipoDePapelNoCondominio.SINDICO);
  }

  private boolean possuiPapelNoCondominio(Condominio condominio, TipoDePapelNoCondominio papel) {
    Set<PapelNoCondominio> papeis = getPapeisNoCondominio(condominio);
    Optional<PapelNoCondominio> condomino = papeis.stream().filter(p -> p.getTipo() == papel).findFirst();
    return condomino.isPresent();
  }

  public void acrescentaCondominio(UsuarioNoCondominio usuarioNoCondominio) {
    getCondominiosDoUsuario().add(usuarioNoCondominio);
  }

  public static UsuariosRepository usuarios() {
    return Aplicacao.get().getRepositorio(UsuariosRepository.class);
  }

  public Chat getChatCom(Usuario usuarioSindico) {
    ChatRepository chats = Aplicacao.get().getRepositorio(ChatRepository.class);
    return chats.getChatEntre(this, usuarioSindico);
  }

  public Chat criaChatCom(Usuario usuario, Condominio condominio) {
    return Chat.cria(condominio, this, usuario);
  }

  public static Usuario getPorEmail(String email) {
    UsuariosRepository usuarios = Aplicacao.get().getRepositorio(UsuariosRepository.class);
    return usuarios.getPorEmail(email);
  }
}
