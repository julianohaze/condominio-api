package br.com.tagitta.condominio.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.integracao.hibernate.converter.LocalDateConverter;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.exception.AgendamentoDeMudancaException;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;
import br.com.tagitta.condominio.model.tipo.EstadoDoAgendamentoDeMudanca;
import br.com.tagitta.condominio.model.tipo.Periodo;
import br.com.tagitta.condominio.model.tipo.TipoDeAgendamentoDeMudanca;
import br.com.tagitta.condominio.model.tipo.TipoDeEvento;
import br.com.tagitta.condominio.repository.AgendamentoDeMudancaRepository;

@Entity
@Table
public class AgendamentoDeMudanca extends Entidade<String> {
	private LocalDate data;
	private Periodo periodo;
	private EstadoDoAgendamentoDeMudanca estado;
	private TipoDeAgendamentoDeMudanca tipo;
	private LocalDateTime dataSolicitacao;
	private LocalDateTime dataAutorizacao;
	private LocalDateTime dataRecusa;
	private LocalDateTime dataCancelamento;
	private Evento evento;
	private Condomino condomino;
	private Usuario autorizador;

	private AgendamentoDeMudanca() {
	}

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "agendamentodemudanca_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Column
	@Convert(converter = LocalDateConverter.class)
	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	@Column
	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.PeriodoUserType")
	public Periodo getPeriodo() {
		return periodo;
	}

	protected void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	@Column
	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.EstadoDoAgendamentoDeMudancaUserType")
	public EstadoDoAgendamentoDeMudanca getEstado() {
		return estado;
	}

	public void setEstado(EstadoDoAgendamentoDeMudanca estado) {
		this.estado = estado;
	}

	@Column
	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.TipoDeAgendamentoDeMudancaUserType")
	public TipoDeAgendamentoDeMudanca getTipo() {
		return tipo;
	}

	protected void setTipo(TipoDeAgendamentoDeMudanca tipo) {
		this.tipo = tipo;
	}

	@JsonIgnore
	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataSolicitacao() {
		return dataSolicitacao;
	}

	@SuppressWarnings("unused")
	private void setDataSolicitacao(LocalDateTime dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	@JsonIgnore
	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataAutorizacao() {
		return dataAutorizacao;
	}

	@SuppressWarnings("unused")
	private void setDataAutorizacao(LocalDateTime dataAutorizacao) {
		this.dataAutorizacao = dataAutorizacao;
	}

	@JsonIgnore
	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataRecusa() {
		return dataRecusa;
	}

	public void setDataRecusa(LocalDateTime dataRecusa) {
		this.dataRecusa = dataRecusa;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(LocalDateTime dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	@ManyToOne
	@JoinColumn(name = "evento_id")
	public Evento getEvento() {
		return evento;
	}

	@SuppressWarnings("unused")
	private void setEvento(Evento evento) {
		this.evento = evento;
	}

	@ManyToOne
	@JoinColumn(name = "condomino_id")
	public Condomino getCondomino() {
		return condomino;
	}

	protected void setCondomino(Condomino condomino) {
		this.condomino = condomino;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "autorizador_id")
	public Usuario getAutorizador() {
		return autorizador;
	}

	@SuppressWarnings("unused")
	private void setAutorizador(Usuario autorizador) {
		this.autorizador = autorizador;
	}

	public static AgendamentoDeMudancaBuilder cria() {
		return new AgendamentoDeMudancaBuilder(new AgendamentoDeMudanca());
	}

	public void solicitar() {
		erroSeDadosInvalidos();
		erroSeJaExisteAgendamentoNoMesmoPeriodoEBloco();

		this.estado = EstadoDoAgendamentoDeMudanca.PENDENTE;
		this.dataSolicitacao = LocalDateTime.now();

		Evento evento = new Evento();
		evento.setCondominio(this.condomino.getCondominio());
		evento.setDescricao("Mudança " + this.condomino.getUnidade());
		evento.setData(this.dataSolicitacao);
		evento.setTipo(TipoDeEvento.RESERVA);
		evento.persiste();

		this.evento = evento;

		persiste();
	}

	private void erroSeJaExisteAgendamentoNoMesmoPeriodoEBloco() {
		if (this.condomino.getCondominio().possuiBlocos()) {
			AgendamentoDeMudanca agendamento = repository().get(this.data, this.periodo, this.condomino.getBloco());
			if (agendamento != null) {
				throw new AgendamentoDeMudancaException("Uma mudança já foi agendada para o seu bloco no mesmo período.");
			}
		}
	}

	public static AgendamentoDeMudancaRepository repository() {
		return Aplicacao.get().getRepositorio(AgendamentoDeMudancaRepository.class);
	}

	private void erroSeDadosInvalidos() {
		if (data == null) {
			throw new DadosInvalidosException("A data da mudança deve ser informada!");
		}

		if (tipo == null) {
			throw new DadosInvalidosException("O tipo da mudança deve ser informado!");
		}

		if (periodo == null) {
			throw new DadosInvalidosException("O período da mudança deve ser informado!");
		}
	}

	public void autorizar() {
		Usuario usuario = UsuarioCorrente.get();

		erroSeUsuarioAutorizadorNaoESindico(usuario);

		this.estado = EstadoDoAgendamentoDeMudanca.AUTORIZADA;
		this.dataAutorizacao = LocalDateTime.now();
		this.autorizador = usuario;

		persiste();
	}

	private void erroSeUsuarioAutorizadorNaoESindico(Usuario usuario) {
		if (!usuario.equals(this.condomino.getCondominio().getSindico().getUsuario())) {
			throw new UsuarioNaoESindicoException("Somente o síndico pode autorizar ou recusar uma mudança.");
		}
	}

	public void recusar() {
		Usuario usuario = UsuarioCorrente.get();

		erroSeUsuarioAutorizadorNaoESindico(usuario);

		this.estado = EstadoDoAgendamentoDeMudanca.RECUSADA;
		this.dataRecusa = LocalDateTime.now();
		this.autorizador = usuario;

		persiste();
	}

	public void cancelar() {
		if (estado == EstadoDoAgendamentoDeMudanca.CANCELADA)
			throw new IllegalStateException("Não é possível cancelar uma reserva que já foi cancelada.");

		this.estado = EstadoDoAgendamentoDeMudanca.CANCELADA;
		this.dataCancelamento = LocalDateTime.now();
		persiste();
	}
}