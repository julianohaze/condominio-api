package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

public class MensagemBuilder {

	private Mensagem mensagem;

	public MensagemBuilder(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public MensagemBuilder para(Chat chat) {
		this.mensagem.setChat(chat);
		return this;
	}

	public MensagemBuilder doUsuario(Usuario usuario) {
		this.mensagem.setUsuario(usuario);
		return this;
	}

	public MensagemBuilder comTexto(String texto) {
		this.mensagem.setTexto(texto);
		return this;
	}

	public MensagemBuilder naData(LocalDateTime dataHora) {
		this.mensagem.setDataHora(dataHora);
		return this;
	}
	
	public MensagemBuilder comId(String id) {
		this.mensagem.setId(id);
		return this;
	}

	public Mensagem build() {
		return this.mensagem;
	}
}
