package br.com.tagitta.condominio.model.to;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.tagitta.condominio.model.tipo.Periodo;
import br.com.tagitta.condominio.model.tipo.TipoDeAgendamentoDeMudanca;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SolicitacaoDeMudanca {
	private String id;
	private Periodo periodo;
	private TipoDeAgendamentoDeMudanca tipo;
	private LocalDate data;
	private String condominoId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public TipoDeAgendamentoDeMudanca getTipo() {
		return tipo;
	}

	public void setTipo(TipoDeAgendamentoDeMudanca tipo) {
		this.tipo = tipo;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public String getCondominoId() {
		return condominoId;
	}

	public void setCondominoId(String condominoId) {
		this.condominoId = condominoId;
	}
}