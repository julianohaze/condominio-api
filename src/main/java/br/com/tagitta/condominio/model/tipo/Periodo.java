package br.com.tagitta.condominio.model.tipo;

import br.com.syncode.arquitetura.entidade.EnumeradoPersistente;

public enum Periodo implements EnumeradoPersistente {
	MANHA("MAN", "Manhã"), TARDE("TAR", "Tarde");

	private String id;
	private String descricao;

	private Periodo(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getDescricao() {
		return descricao;
	}
}
