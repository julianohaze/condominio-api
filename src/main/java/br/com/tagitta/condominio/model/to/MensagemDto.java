package br.com.tagitta.condominio.model.to;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.tagitta.condominio.model.Mensagem;
import br.com.tagitta.condominio.model.tipo.EstadoDaMensagem;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MensagemDto {
	private String id;
	private LocalDateTime dataHora;
	private String texto;
	private String usuarioId;
	private String usuarioNome;
	private String chatId;
	private EstadoDaMensagem estado;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getDataHora() {
		return dataHora;
	}

	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getUsuarioNome() {
		return usuarioNome;
	}

	public void setUsuarioNome(String usuarioNome) {
		this.usuarioNome = usuarioNome;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	
	public EstadoDaMensagem getEstado() {
		return estado;
	}
	
	public void setEstado(EstadoDaMensagem estado) {
		this.estado = estado;
	}

	public static MensagemDto from(Mensagem m) {
		MensagemDto mensagem = new MensagemDto();
		mensagem.setId(m.getId());
		mensagem.setDataHora(m.getDataHora());
		mensagem.setTexto(m.getTexto());
		mensagem.setUsuarioId(m.getUsuario().getId());
		mensagem.setUsuarioNome(m.getUsuario().getNome());
		mensagem.setChatId(m.getChat().getId());
		mensagem.setEstado(m.getEstado());
		return mensagem;
	}
}