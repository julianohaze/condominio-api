package br.com.tagitta.condominio.model.tipo;

import br.com.syncode.arquitetura.entidade.EnumeradoPersistente;

public enum EstadoDaReserva implements EnumeradoPersistente {
	AUTORIZADA("AUT", "Autorizada"), PENDENTE("PEN", "Pendente"), RECUSADA("REC", "Recusada"), CANCELADA("CAN", "Cancelada");

	private String id;
	private String descricao;

	private EstadoDaReserva(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
