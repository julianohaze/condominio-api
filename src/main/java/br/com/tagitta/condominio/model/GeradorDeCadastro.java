package br.com.tagitta.condominio.model;

import java.time.LocalDate;

public class GeradorDeCadastro {

	public void gera(CadastroDeCondominio cadastro) {
		Condominio condominio = new Condominio();
		condominio.setNome(cadastro.getNome());
		condominio.setEndereco(cadastro.getEndereco());
		condominio.persiste();
		
		Usuario usuario = new Usuario();
		usuario.setNome(cadastro.getNomeSindico());
		usuario.setEmail(cadastro.getEmail());
		usuario.setSenha(cadastro.getSenha());
		usuario.persiste();
		
		condominio.acrescentaSindico(usuario, LocalDate.now());
		
		cadastro.getBlocos().forEach(bloco -> {
			condominio.acrescentarBloco(bloco);
		});
	}

}
