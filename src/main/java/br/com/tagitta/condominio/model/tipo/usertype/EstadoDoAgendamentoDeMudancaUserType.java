package br.com.tagitta.condominio.model.tipo.usertype;

import br.com.syncode.integracao.hibernate.tipo.EnumUserType;
import br.com.tagitta.condominio.model.tipo.EstadoDoAgendamentoDeMudanca;

public class EstadoDoAgendamentoDeMudancaUserType extends EnumUserType<EstadoDoAgendamentoDeMudanca> {

}
