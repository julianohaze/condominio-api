package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

public class ReservaBuilder {
	private Reserva reserva;

	protected ReservaBuilder() {
		reserva = new Reserva();
	}

	public ReservaBuilder para(Condomino condomino) {
		reserva.setCondomino(condomino);
		return this;
	}

	public ReservaBuilder comDescricao(String descricao) {
		reserva.setDescricao(descricao);
		return this;
	}

	public ReservaBuilder comDataInicial(LocalDateTime dataInicial) {
		reserva.setDataInicial(dataInicial);
		return this;
	}

	public ReservaBuilder comDataFinal(LocalDateTime dataFinal) {
		reserva.setDataFinal(dataFinal);
		return this;
	}

	public ReservaBuilder paraArea(AreaDeReserva areaDeReserva) {
		reserva.setArea(areaDeReserva);
		return this;
	}

	public Reserva build() {
		return reserva;
	}

	public ReservaBuilder paraDiaTodo(boolean diaTodo) {
		reserva.setDiaTodo(diaTodo);
		return this;
	}
}
