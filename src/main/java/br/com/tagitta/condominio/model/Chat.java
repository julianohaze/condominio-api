package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.repository.ChatRepository;

@Entity
public class Chat extends Entidade<String> {
	private LocalDateTime inicio;
	private LocalDateTime termino;
	private LocalDateTime dataHoraDaUltimaMensagem;
	private Mensagem ultimaMensagemRecebida;
	private Condominio condominio;
	private List<Usuario> participantes;

	private Chat() {
	}

	private Chat(Condominio condominio, Usuario usuario1, Usuario usuario2) {
		this();
		this.condominio = condominio;
		acrescentaParticipante(usuario1);
		acrescentaParticipante(usuario2);
	}

	public void acrescentaParticipante(Usuario participante) {
		getParticipantes().add(participante);
	}

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "chat_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Convert(converter = LocalDateTimeConverter.class)
	@Column(nullable = false)
	public LocalDateTime getInicio() {
		return inicio;
	}

	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}

	@Convert(converter = LocalDateTimeConverter.class)
	@Column
	public LocalDateTime getTermino() {
		return termino;
	}

	public void setTermino(LocalDateTime termino) {
		this.termino = termino;
	}
	
	@Convert(converter = LocalDateTimeConverter.class)
	@Column
	public LocalDateTime getDataHoraDaUltimaMensagem() {
		return dataHoraDaUltimaMensagem;
	}

	public void setDataHoraDaUltimaMensagem(LocalDateTime dataHoraDaUltimaMensagem) {
		this.dataHoraDaUltimaMensagem = dataHoraDaUltimaMensagem;
	}

	@ManyToOne
	@JoinColumn(name = "ultimamensagem_id")
	public Mensagem getUltimaMensagemRecebida() {
		return ultimaMensagemRecebida;
	}

	public void setUltimaMensagemRecebida(Mensagem ultimaMensagemRecebida) {
		this.ultimaMensagemRecebida = ultimaMensagemRecebida;
	}

	@ManyToOne
	@JoinColumn(name = "condominio_id", nullable = false)
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@ManyToMany
	@JoinTable(name = "chat_participantes", joinColumns = { @JoinColumn(name = "chat_id") }, inverseJoinColumns = { @JoinColumn(name = "usuario_id") })
	public List<Usuario> getParticipantes() {
		if (participantes == null) {
			participantes = new ArrayList<>();
		}
		return participantes;
	}

	public void setParticipantes(List<Usuario> participantes) {
		this.participantes = participantes;
	}

	public static Chat cria(Condominio condominio, Usuario usuario1, Usuario usuario2) {
		Chat chat = new Chat(condominio, usuario1, usuario2);
		chat.setInicio(LocalDateTime.now());
		chat.persiste();
		return chat;
	}

	public Collection<Mensagem> getMensagensAPartirDe(LocalDateTime data) {
		return repository().getMensagensAPartirDe(this, data);
	}
	
	private static ChatRepository repository() {
		return Aplicacao.get().getRepositorio(ChatRepository.class);
	}
}