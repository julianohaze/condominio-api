package br.com.tagitta.condominio.model.tipo;

import br.com.syncode.arquitetura.entidade.EnumeradoPersistente;

public enum TipoDeEvento implements EnumeradoPersistente {
	RESERVA("RES", "Reserva"), MUDANCA("MUD", "Mudança"), COMUNICADO("COM", "ComunicadoTest");

	private String id;
	private String descricao;

	private TipoDeEvento(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
