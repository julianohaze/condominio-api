package br.com.tagitta.condominio.model;

/**
 * 
 * Armazena o usuário da requisição corrente.
 * 
 * @author julianosilva
 *
 */
public class UsuarioCorrente {
	private static ThreadLocal<Usuario> usuario;

	public static Usuario get() {
		return getUsuario().get();
	}

	public static void set(Usuario usuario) {
		getUsuario().set(usuario);
	}

	public static void clear() {
		getUsuario().remove();
	}

	private static ThreadLocal<Usuario> getUsuario() {
		if (usuario == null) {
			usuario = new ThreadLocal<Usuario>();
		}
		return usuario;
	}
}
