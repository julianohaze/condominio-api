package br.com.tagitta.condominio.model.to;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.tagitta.condominio.model.Chat;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioCorrente;

public class ChatDto {
	private String id;
	private LocalDateTime inicio;
	private LocalDateTime termino;
	private LocalDateTime dataHoraDaUltimaMensagem;
	private String ultimaMensagemRecebida;
	private String condominioid;
	private String participanteId;
	private String participanteNome;
	private Collection<MensagemDto> mensagens;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getInicio() {
		return inicio;
	}

	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}

	public LocalDateTime getTermino() {
		return termino;
	}

	public void setTermino(LocalDateTime termino) {
		this.termino = termino;
	}

	public LocalDateTime getDataHoraDaUltimaMensagem() {
		return dataHoraDaUltimaMensagem;
	}

	public void setDataHoraDaUltimaMensagem(LocalDateTime dataHoraDaUltimaMensagem) {
		this.dataHoraDaUltimaMensagem = dataHoraDaUltimaMensagem;
	}

	public String getUltimaMensagemRecebida() {
		return ultimaMensagemRecebida;
	}

	public void setUltimaMensagemRecebida(String ultimaMensagemRecebida) {
		this.ultimaMensagemRecebida = ultimaMensagemRecebida;
	}

	public String getCondominioid() {
		return condominioid;
	}

	public void setCondominioid(String condominioid) {
		this.condominioid = condominioid;
	}

	public String getParticipanteId() {
		return participanteId;
	}

	public void setParticipanteId(String participanteId) {
		this.participanteId = participanteId;
	}

	public String getParticipanteNome() {
		return participanteNome;
	}

	public void setParticipanteNome(String participanteNome) {
		this.participanteNome = participanteNome;
	}

	public Collection<MensagemDto> getMensagens() {
		if (mensagens == null) {
			mensagens = new ArrayList<>();
		}
		return mensagens;
	}

	public void setMensagens(Collection<MensagemDto> mensagens) {
		this.mensagens = mensagens;
	}

	private void acrescentaMensagem(MensagemDto mensagem) {
		getMensagens().add(mensagem);
	}

	public static ChatDto from(Chat chat, LocalDateTime ultimaAtualizacao) {
		Usuario usuarioCorrente = UsuarioCorrente.get();

		ChatDto dto = new ChatDto();
		dto.setId(chat.getId());
		dto.setCondominioid(chat.getCondominio().getId());
		dto.setUltimaMensagemRecebida(chat.getUltimaMensagemRecebida() != null ? chat.getUltimaMensagemRecebida().getTexto() : "");
		dto.setInicio(chat.getInicio());
		dto.setTermino(chat.getTermino());
		dto.setDataHoraDaUltimaMensagem(chat.getDataHoraDaUltimaMensagem());

		chat.getParticipantes().forEach(p -> {
			if (!p.equals(usuarioCorrente)) {
				dto.setParticipanteId(p.getId());
				dto.setParticipanteNome(p.getNome());
			}
		});

		chat.getMensagensAPartirDe(ultimaAtualizacao).forEach(m -> {
			MensagemDto mensagem = MensagemDto.from(m);
			dto.acrescentaMensagem(mensagem);
		});
		return dto;
	}

	public static Collection<ChatDto> from(Collection<Chat> chats, LocalDateTime ultimaAtualizacao) {
		List<ChatDto> lista = new ArrayList<ChatDto>();
		for (Chat chat : chats) {
			lista.add(from(chat, ultimaAtualizacao));
		}
		return lista;
	}
}
