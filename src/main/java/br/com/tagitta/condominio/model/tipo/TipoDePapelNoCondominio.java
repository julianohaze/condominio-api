package br.com.tagitta.condominio.model.tipo;

public enum TipoDePapelNoCondominio {
	CONDOMINO("Condômino"), SINDICO("Síndico"), ADMINISTRADOR("Administrador");

	private String nomeApresentavel;

	private TipoDePapelNoCondominio(String nomeApresentavel) {
		this.nomeApresentavel = nomeApresentavel;
	}

	public String getNomeApresentavel() {
		return nomeApresentavel;
	}
}
