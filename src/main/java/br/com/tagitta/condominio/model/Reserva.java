package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.ReservaComDatasConflitantesException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;
import br.com.tagitta.condominio.model.tipo.EstadoDaReserva;
import br.com.tagitta.condominio.model.tipo.TipoDeEvento;
import br.com.tagitta.condominio.repository.ReservasRepository;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Reserva extends Entidade<String> {
	private String descricao;
	private EstadoDaReserva estado;
	private boolean diaTodo;
	private LocalDateTime dataInicial;
	private LocalDateTime dataFinal;
	private LocalDateTime dataAutorizacao;
	private LocalDateTime dataCancelamento;
	private LocalDateTime dataRecusa;
	private LocalDateTime dataSolicitacao;
	private Condomino condomino;
	private Usuario autorizador;
	private Evento evento;
	private AreaDeReserva area;

	protected Reserva() {
	}

	public static ReservaBuilder cria() {
		return new ReservaBuilder();
	}

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "reserva_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Column(length = 100)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Column(length = 3)
	@Type(type = "br.com.tagitta.condominio.model.tipo.usertype.EstadoDaReservaUserType")
	public EstadoDaReserva getEstado() {
		return estado;
	}

	@SuppressWarnings("unused")
	private void setEstado(EstadoDaReserva estado) {
		this.estado = estado;
	}

	@Column
	public boolean isDiaTodo() {
		return diaTodo;
	}

	public void setDiaTodo(boolean diaTodo) {
		this.diaTodo = diaTodo;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataInicial() {
		return dataInicial;
	}

	protected void setDataInicial(LocalDateTime dataInicial) {
		this.dataInicial = dataInicial;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataFinal() {
		return dataFinal;
	}

	protected void setDataFinal(LocalDateTime dataFinal) {
		this.dataFinal = dataFinal;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataAutorizacao() {
		return dataAutorizacao;
	}

	@SuppressWarnings("unused")
	private void setDataAutorizacao(LocalDateTime dataAutorizacao) {
		this.dataAutorizacao = dataAutorizacao;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataRecusa() {
		return dataRecusa;
	}

	@SuppressWarnings("unused")
	private void setDataRecusa(LocalDateTime dataRecusa) {
		this.dataRecusa = dataRecusa;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataCancelamento() {
		return dataCancelamento;
	}

	@SuppressWarnings("unused")
	private void setDataCancelamento(LocalDateTime dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getDataSolicitacao() {
		return dataSolicitacao;
	}

	@SuppressWarnings("unused")
	private void setDataSolicitacao(LocalDateTime dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	@ManyToOne
	@JoinColumn(name = "condominio_id")
	public Condomino getCondomino() {
		return condomino;
	}

	public void setCondomino(Condomino condomino) {
		this.condomino = condomino;
	}

	@ManyToOne
	@JoinColumn(name = "autorizador_id")
	public Usuario getAutorizador() {
		return autorizador;
	}

	public void setAutorizador(Usuario autorizador) {
		this.autorizador = autorizador;
	}

	@ManyToOne
	@JoinColumn(name = "evento_id")
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	@ManyToOne
	@JoinColumn(name = "area_id", nullable = false)
	public AreaDeReserva getArea() {
		return area;
	}

	public void setArea(AreaDeReserva area) {
		this.area = area;
	}

	public void registra() {
		if (diaTodo) {
			dataInicial = dataInicial.withHour(0).withMinute(0).withSecond(0);
			dataFinal = dataInicial.withHour(23).withMinute(59).withSecond(59);
		}

		erroSeDadosInvalidos();
		erroSeJaExisteReservaComDatasConflitantes();

		this.estado = EstadoDaReserva.PENDENTE;
		this.dataSolicitacao = LocalDateTime.now();

		Evento evento = new Evento();
		evento.setCondominio(this.condomino.getCondominio());
		evento.setDescricao(this.descricao);
		evento.setData(this.dataInicial);
		evento.setTipo(TipoDeEvento.RESERVA);
		evento.persiste();

		this.evento = evento;

		persiste();
	}

	private void erroSeDadosInvalidos() {
		if (area == null)
			throw new DadosInvalidosException("A área da reserva deve ser informada!");

		if (dataInicial == null)
			throw new DadosInvalidosException("A data inicial da reserva deve ser informada!");

		if (!diaTodo && dataFinal == null)
			throw new DadosInvalidosException("A data final da reserva deve ser informada!");
	}

	private void erroSeJaExisteReservaComDatasConflitantes() {
		Collection<Reserva> reservas = reservas().daAreaNoDia(this.area, this.dataInicial.toLocalDate());
		for (Reserva reserva : reservas) {
			if (reserva.coincideCom(this)) {
				throw new ReservaComDatasConflitantesException(this);
			}
		}
	}

	private boolean coincideCom(Reserva reserva) {
		return this.dataFinal.compareTo(reserva.dataInicial) >= 0 || this.dataInicial.compareTo(reserva.dataFinal) >= 0;
	}

	public static ReservasRepository reservas() {
		return Aplicacao.get().getRepositorio(ReservasRepository.class);
	}

	public void autoriza() {
		Usuario usuario = UsuarioCorrente.get();

		erroSeUsuarioAutorizadorNaoESindico(usuario);

		this.estado = EstadoDaReserva.AUTORIZADA;
		this.setAutorizador(usuario);
		this.dataAutorizacao = LocalDateTime.now();
		persiste();
	}

	private void erroSeUsuarioAutorizadorNaoESindico(Usuario usuario) {
		if (!usuario.equals(this.condomino.getCondominio().getSindico().getUsuario())) {
			throw new UsuarioNaoESindicoException("Somente o síndico pode autorizar ou recusar uma reserva.");
		}
	}

	public void recusa() {
		if (this.estado != EstadoDaReserva.PENDENTE)
			throw new IllegalStateException("Uma reserva " + this.estado.getDescricao() + " não pode ser cancelada.");

		Usuario usuario = UsuarioCorrente.get();

		erroSeUsuarioAutorizadorNaoESindico(usuario);

		this.estado = EstadoDaReserva.RECUSADA;
		this.setAutorizador(usuario);
		this.dataRecusa = LocalDateTime.now();
		persiste();
	}

	public void cancela() {
		if (this.estado != EstadoDaReserva.AUTORIZADA && this.estado != EstadoDaReserva.PENDENTE)
			throw new IllegalStateException("Uma reserva " + this.estado.getDescricao() + " não pode ser cancelada.");

		this.estado = EstadoDaReserva.CANCELADA;
		this.dataCancelamento = LocalDateTime.now();
		persiste();
	}
}