package br.com.tagitta.condominio.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;

@Entity
@DiscriminatorValue("adm")
public class Administrador extends PapelNoCondominio {
	private Set<Papel> papeis;

	public void acrescentaPapel(Papel papel) {
		getPapeis().add(papel);
	}

	public void retiraPapel(Papel papel) {
		getPapeis().remove(papel);
	}

	@ManyToMany()
	@JoinTable(name = "administrador_papel", joinColumns = { @JoinColumn(name = "administrador_id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "papel_id") })
	public Set<Papel> getPapeis() {
		if (papeis == null) {
			papeis = new HashSet<>();
		}
		return papeis;
	}

	public void setPapeis(Set<Papel> papeis) {
		this.papeis = papeis;
	}

	@Transient
	@Override
	public TipoDePapelNoCondominio getTipo() {
		return TipoDePapelNoCondominio.ADMINISTRADOR;
	}
}