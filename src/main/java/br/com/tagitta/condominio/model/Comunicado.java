package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import br.com.syncode.arquitetura.entidade.Entidade;
import br.com.syncode.arquitetura.util.UtilString;
import br.com.syncode.integracao.hibernate.converter.LocalDateTimeConverter;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;
import br.com.tagitta.condominio.model.tipo.TipoDeEvento;

@Entity
public class Comunicado extends Entidade<String> {
	private LocalDateTime data;
	private String titulo;
	private String texto;
	private Condominio condominio;
	private Sindico sindico;
	private Evento evento;

	@Id
	@GeneratedValue(generator = "UUIDGenerator")
	@GenericGenerator(name = "UUIDGenerator", strategy = "base.util.UUIDGenerator")
	@Column(name = "comunicado_id", length = 32)
	@Override
	public String getId() {
		return super.getId();
	}

	@Column
	@Convert(converter = LocalDateTimeConverter.class)
	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	@Column(length = 50, nullable = false)
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Column(nullable = false)
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	@ManyToOne
	@JoinColumn(name = "condominio_id")
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@ManyToOne
	@JoinColumn(name = "sindico_id")
	public Sindico getSindico() {
		return sindico;
	}

	public void setSindico(Sindico sindico) {
		this.sindico = sindico;
	}

	@ManyToOne
	@JoinColumn(name = "evento_id")
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	@Override
	public void persiste() {
		erroSeDadosInvalidos();

		if (getId() == null) {
			Usuario usuario = UsuarioCorrente.get();
			erroSeUsuarioNaoESindico(usuario);

			this.setSindico(condominio.getSindico());
			this.setData(LocalDateTime.now());

			Evento evento = new Evento();
			evento.setCondominio(this.condominio);
			evento.setDescricao(this.titulo);
			evento.setData(this.data);
			evento.setTipo(TipoDeEvento.RESERVA);
			evento.persiste();

			this.evento = evento;
		}
		super.persiste();
	}

	private void erroSeDadosInvalidos() {
		if (UtilString.isVazio(this.titulo))
			throw new DadosInvalidosException("O título do comunicado deve ser informado!");

		if (UtilString.isVazio(this.texto))
			throw new DadosInvalidosException("O texto do comunicado deve ser informado!");
	}

	private void erroSeUsuarioNaoESindico(Usuario usuario) {
		if (!usuario.equals(condominio.getSindico().getUsuario())) {
			throw new UsuarioNaoESindicoException("Somente o síndico pode criar comunicados.");
		}
	}
}