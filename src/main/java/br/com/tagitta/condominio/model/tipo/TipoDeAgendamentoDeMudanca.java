package br.com.tagitta.condominio.model.tipo;

import br.com.syncode.arquitetura.entidade.EnumeradoPersistente;

public enum TipoDeAgendamentoDeMudanca implements EnumeradoPersistente {
	ENTRAR("ENT", "Entrar"), SAIR("SAI", "Sair");

	private String id;
	private String descricao;

	private TipoDeAgendamentoDeMudanca(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}
}