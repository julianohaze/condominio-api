package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;
import br.com.tagitta.condominio.model.Reserva;

public class ReservaComDatasConflitantesException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReservaComDatasConflitantesException(Reserva reserva) {
		super(criaMensagem(reserva));
	}

	private static String criaMensagem(Reserva reserva) {
		StringBuilder msg = new StringBuilder();
		msg.append("Já foi feita uma reserva para ");
		msg.append(reserva.getArea().getDescricao());
		msg.append(" que coincide com o período que você informou. Consulte a agenda para ver a disponibilidade.");
		return msg.toString();
	}
}
