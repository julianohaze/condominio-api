package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;

public class UsuarioNaoESindicoException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNaoESindicoException(String message) {
		super(message);
	}

}
