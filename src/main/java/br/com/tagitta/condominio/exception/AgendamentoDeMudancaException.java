package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;

public class AgendamentoDeMudancaException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AgendamentoDeMudancaException(String mensagem) {
		super(mensagem);
	}
}
