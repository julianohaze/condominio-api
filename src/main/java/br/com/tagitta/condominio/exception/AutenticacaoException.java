package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;

public class AutenticacaoException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AutenticacaoException(String mensagem) {
		super(mensagem);
	}

}
