package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;

public class DadosInvalidosException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DadosInvalidosException(String mensagem) {
		super(mensagem);
	}
}
