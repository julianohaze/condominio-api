package br.com.tagitta.condominio.exception;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;

public class UsuarioJaPossuiPapelNoCondominioException extends ExcecaoRuntime {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioJaPossuiPapelNoCondominioException(Usuario usuario, Condominio condominio, TipoDePapelNoCondominio papel) {
		super(criaMensagem(usuario, condominio, papel));
	}

	private static String criaMensagem(Usuario usuario, Condominio condominio, TipoDePapelNoCondominio papel) {
		StringBuilder msg = new StringBuilder();
		msg.append(usuario.getNome());
		msg.append(" já é ");
		msg.append(papel.getNomeApresentavel());
		msg.append(" no condomínio ");
		msg.append(condominio.getNome());
		return msg.toString();
	}

}
