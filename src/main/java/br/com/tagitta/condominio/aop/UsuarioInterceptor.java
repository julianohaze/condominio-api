package br.com.tagitta.condominio.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import base.util.annotation.Anonimo;
import br.com.syncode.arquitetura.excecao.ExcecaoUsuarioNaoAutenticado;
import br.com.syncode.arquitetura.util.UtilString;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioCorrente;
import br.com.tagitta.condominio.repository.UsuariosRepository;

@Aspect
@Component
public class UsuarioInterceptor {

	@Autowired(required = true)
	private HttpServletRequest request;

	@Autowired
	private UsuariosRepository usuarios;

	@Around("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public Object processar(ProceedingJoinPoint pjp) throws Throwable {
		try {
			if (isAnonima(pjp)) {
				return pjp.proceed();
			}

			String hash = request.getHeader("hash");
			if (!UtilString.isVazio(hash)) {
				Usuario usuario = usuarios.getPeloHash(hash);
				if (usuario != null) {
					UsuarioCorrente.set(usuario);
					return pjp.proceed();
				}
			}

			throw new ExcecaoUsuarioNaoAutenticado("Usuário não autenticado.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			UsuarioCorrente.clear();

		}
	}

	private boolean isAnonima(ProceedingJoinPoint pjp) {
		Signature sig = pjp.getSignature();
		// tenta obter da classe
		Anonimo anonimo = AnnotationUtils.findAnnotation(sig.getDeclaringType(), Anonimo.class);
		if (anonimo == null) {
			// tenta obter do método
			anonimo = AnnotationUtils.findAnnotation(((MethodSignature) sig).getMethod(), Anonimo.class);
		}

		return anonimo != null;
	}
}
