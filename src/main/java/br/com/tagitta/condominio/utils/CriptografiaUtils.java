package br.com.tagitta.condominio.utils;

import java.security.MessageDigest;

import br.com.syncode.arquitetura.excecao.ExcecaoRuntime;

public class CriptografiaUtils {

	public static String criptografa(String senha) {
		try {
			MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
			byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));

			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
				hexString.append(String.format("%02X", 0xFF & b));
			}
			return hexString.toString();
		} catch (Exception e) {
			throw new ExcecaoRuntime("Erro ao criptografar senha: " + e.getMessage() + ".", e);
		}
	}
}
