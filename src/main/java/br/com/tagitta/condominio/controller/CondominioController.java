package br.com.tagitta.condominio.controller;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import base.infra.util.Dto;
import base.util.annotation.Anonimo;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.CadastroDeCondominio;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.GeradorDeCadastro;
import br.com.tagitta.condominio.model.to.BlocoDto;

@RestController
@RequestMapping("/condominio")
public class CondominioController extends ControladorBase {

  @RequestMapping(value = "/cadastra", method = RequestMethod.POST)
  @Anonimo
  @Transactional
  public @ResponseBody Dto cadastra(@RequestBody CadastroDeCondominio cadastro) {
    GeradorDeCadastro gerador = new GeradorDeCadastro();
    gerador.gera(cadastro);
    return sucesso();
  }

  @RequestMapping(value = "/bloco", method = RequestMethod.POST)
  @Anonimo
  @Transactional
  public @ResponseBody String acrescentarBloco(@RequestBody BlocoDto dto) {
    Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, dto.getCondominioId(), "condominio");
    levantaExcecaoSeTemErro();

    Bloco bloco = new Bloco(dto.getDescricao());
    condominio.acrescentarBloco(bloco);
    return bloco.getId();
  }


  @Anonimo
  @RequestMapping(value = "/todos", method = RequestMethod.GET)
  public Collection<Condominio> getTodos() {
    return dao().getObjetos(Condominio.class);
  }
}
