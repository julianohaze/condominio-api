
package br.com.tagitta.condominio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import base.util.annotation.Anonimo;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.to.CadastroCondomino;

@Controller
@RequestMapping("/condomino")
public class CondominoController extends ControladorBase{

	@RequestMapping(value = "/incluir", method = RequestMethod.POST)
	@Anonimo
	@Transactional
	public @ResponseBody Condomino incluir(@RequestBody CadastroCondomino cadastro) {
		erroSeIndefinidoOuInexistente(Condominio.class, cadastro.getCondominioId(), "condominio");
		erroSeDefinidoEInexistente(Bloco.class, cadastro.getBlocoId(), "bloco");
		levantaExcecaoSeTemErro();
		
		return cadastro.toCondomino();
	}
}
