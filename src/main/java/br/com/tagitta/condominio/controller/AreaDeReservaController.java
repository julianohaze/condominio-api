package br.com.tagitta.condominio.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import base.util.annotation.Anonimo;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.to.AreaDeReservaDto;
import br.com.tagitta.condominio.repository.AreaDeReservaRepository;

@RestController
@RequestMapping("/area")
public class AreaDeReservaController extends ControladorBase {

  @Autowired
  private AreaDeReservaRepository areas;

  @RequestMapping(value = "/all/{condominioId}", method = RequestMethod.GET)
  public @ResponseBody Collection<AreaDeReserva> all(@PathVariable("condominioId") String condominioId) {
    Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominioId, "condominio");
    levantaExcecaoSeTemErro();

    return areas.porCondominio(condominio);
  }

  @Transactional
  @Anonimo
  @RequestMapping(value = "/incluir", method = RequestMethod.POST)
  public @ResponseBody AreaDeReserva all(@RequestBody AreaDeReservaDto dto) {
    erroSeVazio(dto.getDescricao(), "A descrição precisa ser informada.");
    Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, dto.getCondominioId(), "condominio");
    levantaExcecaoSeTemErro();

    AreaDeReserva area = new AreaDeReserva(dto.getDescricao(), condominio);
    area.persiste();
    return area;
  }
}
