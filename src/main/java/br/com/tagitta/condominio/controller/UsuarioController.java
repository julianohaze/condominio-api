package br.com.tagitta.condominio.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import base.infra.util.Dto;
import base.util.annotation.Anonimo;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.exception.AutenticacaoException;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.to.UsuarioAutenticacao;
import br.com.tagitta.condominio.repository.UsuariosRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController extends ControladorBase {

  @Autowired
  private UsuariosRepository usuarios;

  @RequestMapping(value = "/autentica", method = RequestMethod.POST)
  @Anonimo
  @Transactional
  public @ResponseBody Usuario autentica(@RequestBody UsuarioAutenticacao usuario) {
    Usuario usuarioPorEmail = usuarios.getPorEmail(usuario.getEmail());
    if (usuarioPorEmail == null)
      throw new AutenticacaoException("Usuário ou senha inválidos!");

    usuarioPorEmail.autentica(usuario);

    return usuarioPorEmail;
  }

  @RequestMapping(value = "/excluir", method = RequestMethod.POST)
  @Anonimo
  @Transactional
  public @ResponseBody Dto excluir(@RequestBody Usuario dto) {
    Assert.notNull(dto.getId(), "Informe o id do usuário!");

    Usuario usuario = dao().getObjeto(Usuario.class, dto.getId());
    usuario.exclui();

    return sucesso();
  }

  @Anonimo
  @RequestMapping(value = "/todos", method = RequestMethod.GET)
  public Collection<Usuario> getTodos() {
    return dao().getObjetos(Usuario.class);
  }
}
