package br.com.tagitta.condominio.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import base.infra.util.Dto;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Reserva;
import br.com.tagitta.condominio.model.to.RegistroDeReserva;
import br.com.tagitta.condominio.repository.ReservasRepository;

@Controller
@RequestMapping("/reserva")
public class ReservaController extends ControladorBase {

	@Autowired
	private ReservasRepository reservas;

	@RequestMapping(value = "/dias/{mes}", method = RequestMethod.GET)
	public @ResponseBody Collection<LocalDateTime> dias(@PathVariable("mes") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate mes) {
		return reservas.getDiasReservadosNoMes(null, mes);
	}

	@RequestMapping(value = "/dias/{mes}/{areaId}", method = RequestMethod.GET)
	public @ResponseBody Collection<LocalDateTime> dias(@PathVariable("mes") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate mes,
			@PathVariable("areaId") String areaId) {
		AreaDeReserva area = erroSeDefinidoEInexistente(AreaDeReserva.class, areaId, "area");
		levantaExcecaoSeTemErro();

		return reservas.getDiasReservadosNoMes(area, mes);
	}

	@RequestMapping(value = "/get/{reservaId}", method = RequestMethod.GET)
	public @ResponseBody Reserva get(@PathVariable("reservaId") String reservaId) {
		return dao().getObjeto(Reserva.class, reservaId);
	}

	@RequestMapping(value = "/pendentes/{condominioId}", method = RequestMethod.GET)
	public @ResponseBody Collection<Reserva> pendentes(@PathVariable("condominioId") String condominioId) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominioId, "condominio");
		levantaExcecaoSeTemErro();

		return reservas.pendentesDo(condominio);
	}

	@RequestMapping(value = "/registra", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Reserva registra(@RequestBody RegistroDeReserva registro) {
		Condomino condomino = erroSeIndefinidoOuInexistente(Condomino.class, registro.getCondominoId(), "condomino");
		AreaDeReserva area = erroSeIndefinidoOuInexistente(AreaDeReserva.class, registro.getAreaId(), "area");
		levantaExcecaoSeTemErro();

		Reserva reserva = Reserva.cria().comDescricao(registro.getDescricao()).comDataInicial(registro.getDataInicial())
				.comDataFinal(registro.getDataFinal()).para(condomino).paraArea(area).paraDiaTodo(registro.isDiaTodo()).build();
		reserva.registra();

		return reserva;
	}

	@RequestMapping(value = "/autorizar", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Reserva autorizar(@RequestBody RegistroDeReserva registro) {
		Reserva reserva = erroSeIndefinidoOuInexistente(Reserva.class, registro.getId(), "reserva");
		levantaExcecaoSeTemErro();

		reserva.autoriza();

		return reserva;
	}

	@RequestMapping(value = "/recusar", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Reserva recusar(@RequestBody RegistroDeReserva registro) {
		Reserva reserva = erroSeIndefinidoOuInexistente(Reserva.class, registro.getId(), "reserva");
		levantaExcecaoSeTemErro();

		reserva.recusa();

		return reserva;
	}

	@RequestMapping(value = "/all/{condominoId}", method = RequestMethod.GET)
	public @ResponseBody Collection<Reserva> all(@PathVariable("condominoId") String condominoId) {
		Condomino condomino = erroSeIndefinidoOuInexistente(Condomino.class, condominoId, "condomino");
		levantaExcecaoSeTemErro();

		return reservas.getAll(condomino);
	}

	@RequestMapping(value = "/porAreaEData/{areaId}/{data}", method = RequestMethod.GET)
	public @ResponseBody Collection<Reserva> porAreaEDia(@PathVariable("areaId") String areaId,
			@PathVariable("data") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate data) {
		AreaDeReserva area = erroSeIndefinidoOuInexistente(AreaDeReserva.class, areaId, "area");
		levantaExcecaoSeTemErro();

		return reservas.daAreaNoDia(area, data);
	}

	@RequestMapping(value = "/porCondominioEData/{condominioId}/{data}", method = RequestMethod.GET)
	public @ResponseBody Collection<Reserva> porCondominioEDia(@PathVariable("condominioId") String condominoId,
			@PathVariable("data") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate data) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominoId, "condominio");
		levantaExcecaoSeTemErro();

		return reservas.doCondominioNoDia(condominio, data);
	}

	@RequestMapping(value = "/{reservaId}", method = RequestMethod.DELETE)
	@Transactional
	public @ResponseBody Dto cancelar(@PathVariable("reservaId") String reservaId) {
		Reserva reserva = erroSeIndefinidoOuInexistente(Reserva.class, reservaId, "reserva");
		levantaExcecaoSeTemErro();

		reserva.cancela();

		return sucesso();
	}
}
