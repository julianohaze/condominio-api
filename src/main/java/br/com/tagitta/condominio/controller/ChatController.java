package br.com.tagitta.condominio.controller;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import base.infra.util.Dto;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.Chat;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Mensagem;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.to.ChatDto;
import br.com.tagitta.condominio.model.to.MensagemDto;
import br.com.tagitta.condominio.repository.ChatRepository;

@Controller
@RequestMapping("/chat")
public class ChatController extends ControladorBase {

	@Autowired
	private ChatRepository chats;

	@RequestMapping(value = "/all/{condominioId}/{usuarioId}", method = RequestMethod.GET)
	@Transactional
	public @ResponseBody Collection<ChatDto> all(@PathVariable("condominioId") String condominioId, @PathVariable("usuarioId") String usuarioId) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominioId, "condominio");
		Usuario usuario = erroSeIndefinidoOuInexistente(Usuario.class, usuarioId, "usuario");
		levantaExcecaoSeTemErro();

		Collection<Chat> all = chats.all(condominio, usuario, null);
		return ChatDto.from(all, LocalDateTime.now().minusDays(30));
	}

	@RequestMapping(value = "/aposAData/{condominioId}/{usuarioId}/{data}", method = RequestMethod.GET)
	@Transactional
	public @ResponseBody Collection<ChatDto> aposAData(@PathVariable("condominioId") String condominioId, @PathVariable("usuarioId") String usuarioId,
			@PathVariable("data") @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss") LocalDateTime ultimaAtualizacao) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominioId, "condominio");
		Usuario usuario = erroSeIndefinidoOuInexistente(Usuario.class, usuarioId, "usuario");
		levantaExcecaoSeTemErro();

		Collection<Chat> all = chats.all(condominio, usuario, ultimaAtualizacao);
		return ChatDto.from(all, ultimaAtualizacao);
	}

	@RequestMapping(value = "/incluirMensagem", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Mensagem incluirMensagem(@RequestBody MensagemDto mensagem) {
		Chat chat = erroSeIndefinidoOuInexistente(Chat.class, mensagem.getChatId(), "chat");
		Usuario usuario = erroSeIndefinidoOuInexistente(Usuario.class, mensagem.getUsuarioId(), "usuario");
		levantaExcecaoSeTemErro();

		Mensagem msg = Mensagem.cria().comId(mensagem.getId()).para(chat).doUsuario(usuario).comTexto(mensagem.getTexto()).naData(LocalDateTime.now()).build();
		msg.inclui();

		return msg;
	}

	@RequestMapping(value = "/entregarMensagem", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Dto entregarMensagem(@RequestBody Dto dto) {
		String mensagemId = (String) dto.get("id");
		Mensagem mensagem = dao().getObjeto(Mensagem.class, mensagemId);
		if (mensagem != null) {
			mensagem.confirmaRecebimento();
		}
		return sucesso();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/lerMensagens", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody Dto lerMensagens(@RequestBody Dto dto) {
		List<LinkedHashMap> ids = (List<LinkedHashMap>) dto.get("mensagens");

		ids.forEach(id -> {
			Mensagem mensagem = dao().getObjeto(Mensagem.class, (String) id.get("id"));
			if (mensagem != null) {
				mensagem.confirmaLeitura();
			}
		});

		return sucesso();
	}
}