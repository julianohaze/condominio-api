package br.com.tagitta.condominio.controller;

import java.time.LocalDate;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import base.infra.util.Dto;
import br.com.syncode.arquitetura.controlador.ControladorBase;
import br.com.tagitta.condominio.model.AgendamentoDeMudanca;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.to.SolicitacaoDeMudanca;
import br.com.tagitta.condominio.repository.AgendamentoDeMudancaRepository;

@Controller
@RequestMapping("/agendamentoMudanca")
public class AgendamentoDeMudancaController extends ControladorBase {

	@Autowired
	private AgendamentoDeMudancaRepository agendamentos;

	@RequestMapping(value = "/dias/{mes}", method = RequestMethod.GET)
	public @ResponseBody Collection<LocalDate> dias(@PathVariable("mes") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate mes) {
		return agendamentos.getDiasNoMes(mes);
	}

	@RequestMapping(value = "/get/{condominoId}", method = RequestMethod.GET)
	public @ResponseBody AgendamentoDeMudanca get(@PathVariable("condominoId") String condominoId) {
		Condomino condomino = erroSeIndefinidoOuInexistente(Condomino.class, condominoId, "condomino");
		levantaExcecaoSeTemErro();

		return agendamentos.atualDoCondomino(condomino);
	}

	@RequestMapping(value = "/pendentes/{condominioId}", method = RequestMethod.GET)
	public @ResponseBody Collection<AgendamentoDeMudanca> pendentes(@PathVariable("condominioId") String condominioId) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominioId, "condominio");
		levantaExcecaoSeTemErro();

		return agendamentos.pendentesDo(condominio);
	}

	@RequestMapping(value = "/solicitar", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody AgendamentoDeMudanca solicitar(@RequestBody SolicitacaoDeMudanca solicitacao) {
		Condomino condomino = erroSeIndefinidoOuInexistente(Condomino.class, solicitacao.getCondominoId(), "condomino");
		levantaExcecaoSeTemErro();

		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().doTipo(solicitacao.getTipo()).naData(solicitacao.getData())
				.noPeriodo(solicitacao.getPeriodo()).para(condomino).build();
		agendamento.solicitar();

		return agendamento;
	}

	@RequestMapping(value = "/autorizar", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody AgendamentoDeMudanca autorizar(@RequestBody SolicitacaoDeMudanca solicitacao) {
		AgendamentoDeMudanca agendamento = erroSeIndefinidoOuInexistente(AgendamentoDeMudanca.class, solicitacao.getId(), "agendamento");
		levantaExcecaoSeTemErro();

		agendamento.autorizar();

		return agendamento;
	}

	@RequestMapping(value = "/recusar", method = RequestMethod.POST)
	@Transactional
	public @ResponseBody AgendamentoDeMudanca recusar(@RequestBody SolicitacaoDeMudanca solicitacao) {
		AgendamentoDeMudanca agendamento = erroSeIndefinidoOuInexistente(AgendamentoDeMudanca.class, solicitacao.getId(), "agendamento");
		levantaExcecaoSeTemErro();

		agendamento.recusar();

		return agendamento;
	}

	@RequestMapping(value = "/all/{condominoId}", method = RequestMethod.GET)
	public @ResponseBody Collection<AgendamentoDeMudanca> all(@PathVariable("condominoId") String condominoId) {
		Condomino condomino = erroSeIndefinidoOuInexistente(Condomino.class, condominoId, "condomino");
		levantaExcecaoSeTemErro();

		return agendamentos.getAll(condomino);
	}

	@RequestMapping(value = "/porCondominioEData/{condominioId}/{data}", method = RequestMethod.GET)
	public @ResponseBody Collection<AgendamentoDeMudanca> porCondominioEDia(@PathVariable("condominioId") String condominoId,
			@PathVariable("data") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate data) {
		Condominio condominio = erroSeIndefinidoOuInexistente(Condominio.class, condominoId, "condominio");
		levantaExcecaoSeTemErro();

		return agendamentos.doCondominioNoDia(condominio, data);
	}

	@RequestMapping(value = "/{agendamentoId}", method = RequestMethod.DELETE)
	@Transactional
	public @ResponseBody Dto cancelar(@PathVariable("agendamentoId") String agendamentoId) {
		AgendamentoDeMudanca agendamento = erroSeIndefinidoOuInexistente(AgendamentoDeMudanca.class, agendamentoId, "agendamento");
		levantaExcecaoSeTemErro();

		agendamento.cancelar();

		return sucesso();
	}
}