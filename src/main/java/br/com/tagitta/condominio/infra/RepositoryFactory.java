package br.com.tagitta.condominio.infra;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.syncode.arquitetura.integracao.FabricaDeRepositorios;
import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.repository.AgendamentoDeMudancaRepository;
import br.com.tagitta.condominio.repository.AreaDeReservaRepository;
import br.com.tagitta.condominio.repository.ChatRepository;
import br.com.tagitta.condominio.repository.CondominiosRepository;
import br.com.tagitta.condominio.repository.ReservasRepository;
import br.com.tagitta.condominio.repository.UsuariosRepository;

@Component("fabricaDeRepositorios")
public class RepositoryFactory extends FabricaDeRepositorios {

	@Autowired
	private UsuariosRepository usuarios;

	@Autowired
	private CondominiosRepository condominios;

	@Autowired
	private ReservasRepository reservas;

	@Autowired
	private AreaDeReservaRepository areaDeReserva;

	@Autowired
	private AgendamentoDeMudancaRepository agendamentoDeMudanca;

	@Autowired
	private ChatRepository chat;
	
	@Override
	protected void doPopulaDefinicoesDeRepositorio(Map<Class<? extends Repositorio>, Repositorio> repositorios) {
		repositorios.put(UsuariosRepository.class, usuarios);
		repositorios.put(CondominiosRepository.class, condominios);
		repositorios.put(ReservasRepository.class, reservas);
		repositorios.put(AreaDeReservaRepository.class, areaDeReserva);
		repositorios.put(AgendamentoDeMudancaRepository.class, agendamentoDeMudanca);
		repositorios.put(ChatRepository.class, chat);
	}
}