package br.com.tagitta.condominio.infra;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import br.com.syncode.arquitetura.mensagem.FabricaDeMensagens;
import br.com.syncode.arquitetura.util.FabricaArquivo;

@Component("fabricaDeMensagens")
public class MessageFactory extends FabricaDeMensagens {
	private Properties propriedades;

	@Override
	protected Properties doGetPropriedades() {
		if (propriedades == null) {
			ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			try {
				Resource[] resources = resolver.getResources("classpath*:/mensagens.properties");
				propriedades = FabricaArquivo.getInstance().criaProperties(resources[0].getFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return propriedades;
	}

}
