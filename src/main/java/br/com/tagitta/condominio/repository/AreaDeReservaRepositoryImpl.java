package br.com.tagitta.condominio.repository;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;

@Repository
public class AreaDeReservaRepositoryImpl extends RepositorioHibernateBase implements AreaDeReservaRepository {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AreaDeReserva> porCondominio(Condominio condominio) {
		Criteria criteria = criaCriteria(AreaDeReserva.class);
		criteria.add(Restrictions.eq("condominio", condominio));
		criteria.addOrder(Order.asc("descricao"));
		return criteria.list();
	}

}
