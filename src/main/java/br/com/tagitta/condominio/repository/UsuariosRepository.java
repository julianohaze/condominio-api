package br.com.tagitta.condominio.repository;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.Usuario;

public interface UsuariosRepository extends Repositorio {

	Usuario getPorEmail(String email);

	Usuario getPeloHash(String hash);
}
