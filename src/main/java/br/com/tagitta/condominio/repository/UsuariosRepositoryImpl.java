package br.com.tagitta.condominio.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.Usuario;

@Repository("usuariosRepository")
public class UsuariosRepositoryImpl extends RepositorioHibernateBase implements UsuariosRepository {

	@Override
	public Usuario getPorEmail(String email) {
		Criteria criteria = criaCriteria(Usuario.class);
		criteria.add(Restrictions.eq("email", email));
		return (Usuario) criteria.uniqueResult();
	}

	@Override
	public Usuario getPeloHash(String hash) {
		Criteria criteria = criaCriteria(Usuario.class);
		criteria.add(Restrictions.eq("hash", hash));
		return (Usuario) criteria.uniqueResult();
	}
}
