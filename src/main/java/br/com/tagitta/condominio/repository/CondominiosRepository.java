package br.com.tagitta.condominio.repository;

import java.util.Collection;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioNoCondominio;

public interface CondominiosRepository extends Repositorio {

	UsuarioNoCondominio getUsuarioNoCondominio(Usuario usuario, Condominio condominio);

	Collection<Condomino> getCondominos(Condominio condominio);
}