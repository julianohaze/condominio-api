package br.com.tagitta.condominio.repository;

import java.time.LocalDateTime;
import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.Chat;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Mensagem;
import br.com.tagitta.condominio.model.Usuario;

@Repository
public class ChatRepositoryImpl extends RepositorioHibernateBase implements ChatRepository {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Chat> all(Condominio condominio, Usuario usuario, LocalDateTime ultimaAtualizacao) {
		StringBuilder hql = new StringBuilder();
		hql.append("select c ");
		hql.append("from Chat c ");
		hql.append("where c.condominio = :condominio ");
		hql.append("and :usuario in elements (c.participantes) ");
		hql.append("and c.termino is null ");
		if (ultimaAtualizacao != null) {
			hql.append("and c.dataHoraDaUltimaMensagem > :ultimaAtualizacao ");
		}

		hql.append("order by c.dataHoraDaUltimaMensagem ");

		Query query = criaQuery(hql.toString());
		query.setParameter("condominio", condominio);
		query.setParameter("usuario", usuario);
		if (ultimaAtualizacao != null) {
			query.setParameter("ultimaAtualizacao", ultimaAtualizacao);
		}
		return query.list();
	}

	@Override
	public Chat getChatEntre(Usuario usuario1, Usuario usuario2) {
		StringBuilder hql = new StringBuilder();
		hql.append("select c ");
		hql.append("from Chat c ");
		hql.append("where c.termino is null ");
		hql.append("and :usuario1 in elements (c.participantes) ");
		hql.append("and :usuario2 in elements (c.participantes) ");
		hql.append("order by c.dataHoraDaUltimaMensagem desc");

		Query query = criaQuery(hql.toString());
		query.setParameter(":usuario1", usuario1);
		query.setParameter(":usuario2", usuario2);
		query.setMaxResults(1);
		return (Chat) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Mensagem> getMensagensAPartirDe(Chat chat, LocalDateTime data) {
		Criteria criteria = criaCriteria(Mensagem.class);
		criteria.add(Restrictions.eq("chat", chat));
		criteria.add(Restrictions.gt("dataDeAtualizacao", data));
		criteria.addOrder(Order.asc("dataHora"));
		return criteria.list();
	}

	
}
