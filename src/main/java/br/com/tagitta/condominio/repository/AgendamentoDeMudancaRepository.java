package br.com.tagitta.condominio.repository;

import java.time.LocalDate;
import java.util.Collection;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.AgendamentoDeMudanca;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.tipo.Periodo;

public interface AgendamentoDeMudancaRepository extends Repositorio {

	AgendamentoDeMudanca get(LocalDate data, Periodo periodo, Bloco bloco);

	Collection<LocalDate> getDiasNoMes(LocalDate mes);

	Collection<AgendamentoDeMudanca> pendentesDo(Condominio condominio);

	Collection<AgendamentoDeMudanca> getAll(Condomino condomino);

	Collection<AgendamentoDeMudanca> doCondominioNoDia(Condominio condominio, LocalDate data);

	AgendamentoDeMudanca atualDoCondomino(Condomino condomino);
}
