package br.com.tagitta.condominio.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.arquitetura.util.DateUtils;
import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Reserva;
import br.com.tagitta.condominio.model.tipo.EstadoDaReserva;

@Repository("reservasRepository")
public class ReservasRepositoryImpl extends RepositorioHibernateBase implements ReservasRepository {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Reserva> daAreaNoDia(AreaDeReserva area, LocalDate data) {
		LocalDateTime dateTime = LocalDateTime.of(data, LocalTime.MIDNIGHT);
		LocalDateTime dataInicial = dateTime.withHour(0).withMinute(0).withSecond(0);
		LocalDateTime dataFinal = dateTime.withHour(23).withMinute(59).withSecond(59);

		Criteria criteria = criaCriteria(Reserva.class);
		criteria.add(Restrictions.eq("area", area));
		criteria.add(Restrictions.ge("dataInicial", dataInicial));
		criteria.add(Restrictions.le("dataFinal", dataFinal));
		criteria.add(Restrictions.in("estado", new EstadoDaReserva[] { EstadoDaReserva.AUTORIZADA, EstadoDaReserva.PENDENTE }));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<LocalDateTime> getDiasReservadosNoMes(AreaDeReserva area, LocalDate mes) {
		LocalDateTime dataInicial = DateUtils.getFirstDayOfMonth(mes);
		LocalDateTime dataFinal = DateUtils.getLastDayOfMonth(mes).withHour(23).withMinute(59).withSecond(59);

		StringBuilder hql = new StringBuilder();
		hql.append("select distinct ");
		hql.append("	r.dataInicial as dataInicial ");
		hql.append("from Reserva r ");
		hql.append("where r.dataInicial between :dataInicial and :dataFinal ");
		hql.append("and (r.estado = :autorizada or r.estado = :pendente) ");
		if (area != null) {
			hql.append("and r.area = :area ");
		}

		Query query = criaQuery(hql.toString());
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("autorizada", EstadoDaReserva.AUTORIZADA);
		query.setParameter("pendente", EstadoDaReserva.PENDENTE);
		if (area != null) {
			query.setParameter("area", area);
		}
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Reserva> getAll(Condomino condomino) {
		Criteria criteria = criaCriteria(Reserva.class);
		criteria.add(Restrictions.eq("condomino", condomino));
		criteria.add(Restrictions.ge("dataInicial", LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0)));
		criteria.add(Restrictions.in("estado", new EstadoDaReserva[] { EstadoDaReserva.AUTORIZADA, EstadoDaReserva.PENDENTE, EstadoDaReserva.RECUSADA }));
		criteria.addOrder(Order.desc("dataInicial"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Reserva> doCondominioNoDia(Condominio condominio, LocalDate data) {
		LocalDateTime dateTime = LocalDateTime.of(data, LocalTime.MIDNIGHT);
		LocalDateTime dataInicial = dateTime.withHour(0).withMinute(0).withSecond(0);
		LocalDateTime dataFinal = dateTime.withHour(23).withMinute(59).withSecond(59);

		Criteria criteria = criaCriteria(Reserva.class);
		criteria.createAlias("area", "a");
		criteria.add(Restrictions.eq("a.condominio", condominio));
		criteria.add(Restrictions.ge("dataInicial", dataInicial));
		criteria.add(Restrictions.le("dataFinal", dataFinal));
		criteria.add(Restrictions.in("estado", new EstadoDaReserva[] { EstadoDaReserva.AUTORIZADA, EstadoDaReserva.PENDENTE }));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Reserva> pendentesDo(Condominio condominio) {
		Criteria criteria = criaCriteria(Reserva.class);
		criteria.createAlias("area", "a");
		criteria.add(Restrictions.eq("a.condominio", condominio));
		criteria.add(Restrictions.eq("estado", EstadoDaReserva.PENDENTE));
		criteria.addOrder(Order.asc("dataSolicitacao"));
		return criteria.list();
	}
}