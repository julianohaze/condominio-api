package br.com.tagitta.condominio.repository;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioNoCondominio;

@Repository("condominiosRepository")
public class CondominiosRepositoryImpl extends RepositorioHibernateBase implements CondominiosRepository {

	@Override
	public UsuarioNoCondominio getUsuarioNoCondominio(Usuario usuario, Condominio condominio) {
		Criteria criteria = criaCriteria(UsuarioNoCondominio.class);
		criteria.add(Restrictions.eq("usuario", usuario));
		criteria.add(Restrictions.eq("condominio", condominio));
		return (UsuarioNoCondominio) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Condomino> getCondominos(Condominio condominio) {
		Criteria criteria = criaCriteria(Condomino.class);
		criteria.createAlias("usuarioNoCondominio", "usuario");
		criteria.add(Restrictions.eq("usuario.condominio", condominio));
		return criteria.list();
	}
}