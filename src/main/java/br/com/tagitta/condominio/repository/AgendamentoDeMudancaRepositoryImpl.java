package br.com.tagitta.condominio.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.syncode.arquitetura.util.DateUtils;
import br.com.syncode.integracao.hibernate.RepositorioHibernateBase;
import br.com.tagitta.condominio.model.AgendamentoDeMudanca;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.tipo.EstadoDaReserva;
import br.com.tagitta.condominio.model.tipo.EstadoDoAgendamentoDeMudanca;
import br.com.tagitta.condominio.model.tipo.Periodo;

@Repository
public class AgendamentoDeMudancaRepositoryImpl extends RepositorioHibernateBase implements AgendamentoDeMudancaRepository {

	@Override
	public AgendamentoDeMudanca get(LocalDate data, Periodo periodo, Bloco bloco) {
		Criteria criteria = criaCriteria(AgendamentoDeMudanca.class);
		criteria.createAlias("condomino", "condomino");
		criteria.createAlias("condomino.condominio", "condominio");
		criteria.createAlias("condominio.bloco", "bloco");
		criteria.add(Restrictions.eq("bloco", bloco));
		criteria.add(Restrictions.eq("data", data));
		criteria.add(Restrictions.eq("periodo", periodo));
		return (AgendamentoDeMudanca) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<LocalDate> getDiasNoMes(LocalDate mes) {
		LocalDate dataInicial = DateUtils.getFirstDayOfMonth(mes).toLocalDate();
		LocalDate dataFinal = DateUtils.getLastDayOfMonth(mes).toLocalDate();

		StringBuilder hql = new StringBuilder();
		hql.append("select distinct ");
		hql.append(" ag.data ");
		hql.append("from AgendamentoDeMudanca ag ");
		hql.append("where ag.data between :dataInicial and :dataFinal ");
		hql.append("and (ag.estado = :autorizada or ag.estado = :pendente) ");

		Query query = criaQuery(hql.toString());
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("autorizada", EstadoDoAgendamentoDeMudanca.AUTORIZADA);
		query.setParameter("pendente", EstadoDoAgendamentoDeMudanca.PENDENTE);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgendamentoDeMudanca> pendentesDo(Condominio condominio) {
		Criteria criteria = criaCriteria(AgendamentoDeMudanca.class);
		criteria.createAlias("condomino", "condomino");
		criteria.createAlias("condomino.usuarioNoCondominio", "usuarioNoCondominio");
		criteria.add(Restrictions.eq("usuarioNoCondominio.condominio", condominio));
		criteria.add(Restrictions.eq("estado", EstadoDoAgendamentoDeMudanca.PENDENTE));
		criteria.addOrder(Order.asc("dataSolicitacao"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgendamentoDeMudanca> getAll(Condomino condomino) {
		Criteria criteria = criaCriteria(AgendamentoDeMudanca.class);
		criteria.add(Restrictions.eq("condomino", condomino));
		criteria.add(Restrictions.ge("data", LocalDateTime.now().withHour(0).withMinute(0).withSecond(0)));
		criteria.add(Restrictions.in("estado", new EstadoDaReserva[] { EstadoDaReserva.AUTORIZADA, EstadoDaReserva.PENDENTE, EstadoDaReserva.RECUSADA }));
		criteria.addOrder(Order.desc("dataSolicitacao"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgendamentoDeMudanca> doCondominioNoDia(Condominio condominio, LocalDate data) {
		Criteria criteria = criaCriteria(AgendamentoDeMudanca.class);
		criteria.createAlias("condomino", "condomino");
		criteria.createAlias("condomino.usuarioNoCondominio", "usuarioNoCondominio");
		criteria.add(Restrictions.eq("usuarioNoCondominio.condominio", condominio));
		criteria.add(Restrictions.ge("data", data));
		criteria.add(Restrictions.le("data", data));
		criteria.add(Restrictions.in("estado", new EstadoDaReserva[] { EstadoDaReserva.AUTORIZADA, EstadoDaReserva.PENDENTE }));
		return criteria.list();
	}

	@Override
	public AgendamentoDeMudanca atualDoCondomino(Condomino condomino) {
		Criteria criteria = criaCriteria(AgendamentoDeMudanca.class);
		criteria.add(Restrictions.eq("condomino", condomino));
		criteria.add(Restrictions.ge("data", LocalDate.now()));
		criteria.addOrder(Order.desc("dataSolicitacao"));
		criteria.setMaxResults(1);
		return (AgendamentoDeMudanca) criteria.uniqueResult();
	}
}