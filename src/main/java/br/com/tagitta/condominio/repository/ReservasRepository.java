package br.com.tagitta.condominio.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Reserva;

public interface ReservasRepository extends Repositorio {
	Collection<Reserva> daAreaNoDia(AreaDeReserva area, LocalDate data);

	Collection<LocalDateTime> getDiasReservadosNoMes(AreaDeReserva area, LocalDate mes);

	Collection<Reserva> getAll(Condomino condomino);

	Collection<Reserva> doCondominioNoDia(Condominio condominio, LocalDate data);

	Collection<Reserva> pendentesDo(Condominio condominio);
}
