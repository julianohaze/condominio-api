package br.com.tagitta.condominio.repository;

import java.util.Collection;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;

public interface AreaDeReservaRepository extends Repositorio {

	public Collection<AreaDeReserva> porCondominio(Condominio condominio);
}
