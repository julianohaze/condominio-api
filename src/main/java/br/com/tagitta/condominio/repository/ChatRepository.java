package br.com.tagitta.condominio.repository;

import java.time.LocalDateTime;
import java.util.Collection;

import br.com.syncode.arquitetura.integracao.Repositorio;
import br.com.tagitta.condominio.model.Chat;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Mensagem;
import br.com.tagitta.condominio.model.Usuario;

public interface ChatRepository extends Repositorio {

	Collection<Chat> all(Condominio condominio, Usuario usuario, LocalDateTime ultimaAtualizacao);

	Chat getChatEntre(Usuario usuario1, Usuario usuario2);

	Collection<Mensagem> getMensagensAPartirDe(Chat chat, LocalDateTime data);
}
