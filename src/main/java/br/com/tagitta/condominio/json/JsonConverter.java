package br.com.tagitta.condominio.json;

import java.time.LocalDateTime;

import br.com.syncode.arquitetura.json.BaseObjectMapper;
import br.com.syncode.arquitetura.json.LocalDateTimeDeserializer;
import br.com.syncode.arquitetura.json.LocalDateTimeSerializer;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class JsonConverter extends BaseObjectMapper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6043800009310679542L;

	public JsonConverter() {

		super();

//		SimpleModule localDateTimeModule = new SimpleModule("localDateTime");
//		localDateTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer("yyyy-MM-dd'T'HH:mm:ss"));
//		localDateTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer("yyyy-MM-dd'T'HH:mm:ss"));
//		super.registerModule(localDateTimeModule);
	}

}
