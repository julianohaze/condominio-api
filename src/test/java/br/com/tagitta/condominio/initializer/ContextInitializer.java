package br.com.tagitta.condominio.initializer;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class ContextInitializer implements ApplicationContextInitializer<GenericApplicationContext>{

	@Override
	public void initialize(GenericApplicationContext context) {
		context.getEnvironment().getSystemProperties().put(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "test");
	}

}
