package br.com.tagitta.condominio.repository.mock;

import java.time.LocalDate;
import java.util.Collection;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.integracao.DAO;
import br.com.tagitta.condominio.model.AgendamentoDeMudanca;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.tipo.Periodo;
import br.com.tagitta.condominio.repository.AgendamentoDeMudancaRepository;

public class AgendamentoDeMudancaRepositoryMock implements AgendamentoDeMudancaRepository {

	@Override
	public AgendamentoDeMudanca get(LocalDate data, Periodo periodo, Bloco bloco) {
		Collection<AgendamentoDeMudanca> registros = dao().getObjetos(AgendamentoDeMudanca.class);
		for (AgendamentoDeMudanca agendamento : registros) {
			if (agendamento.getCondomino().getBloco().equals(bloco) && agendamento.getPeriodo() == periodo && agendamento.getData().equals(data)) {
				return agendamento;
			}
		}
		return null;
	}

	private DAO dao() {
		return Aplicacao.get().getDAO();
	}

	@Override
	public Collection<LocalDate> getDiasNoMes(LocalDate mes) {
		return null;
	}

	@Override
	public Collection<AgendamentoDeMudanca> pendentesDo(Condominio condominio) {
		return null;
	}

	@Override
	public Collection<AgendamentoDeMudanca> getAll(Condomino condomino) {
		return null;
	}

	@Override
	public Collection<AgendamentoDeMudanca> doCondominioNoDia(Condominio condominio, LocalDate data) {
		return null;
	}

	@Override
	public AgendamentoDeMudanca atualDoCondomino(Condomino condomino) {
		return null;
	}
}