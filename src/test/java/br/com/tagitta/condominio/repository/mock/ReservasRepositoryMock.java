package br.com.tagitta.condominio.repository.mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.integracao.DAO;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Reserva;
import br.com.tagitta.condominio.model.tipo.EstadoDaReserva;
import br.com.tagitta.condominio.repository.ReservasRepository;

public class ReservasRepositoryMock implements ReservasRepository {

	@Override
	public Collection<Reserva> daAreaNoDia(AreaDeReserva area, LocalDate data) {
		LocalDateTime dateTime = LocalDateTime.of(data, LocalTime.MIDNIGHT);
		LocalDateTime dataInicial = dateTime.withHour(0).withMinute(0).withSecond(0);
		LocalDateTime dataFinal = dateTime.withHour(23).withMinute(59).withSecond(59);

		Collection<Reserva> reservas = dao().getObjetos(Reserva.class);
		List<Reserva> filtradas = reservas
				.stream()
				.filter(
						r -> (r.getEstado() == EstadoDaReserva.AUTORIZADA || r.getEstado() == EstadoDaReserva.PENDENTE) && area.equals(r.getArea())
								&& r.getDataInicial().isAfter(dataInicial) && r.getDataFinal().isBefore(dataFinal)).collect(Collectors.toList());
		return filtradas;
	}

	public DAO dao() {
		return Aplicacao.get().getDAO();
	}

	@Override
	public Collection<LocalDateTime> getDiasReservadosNoMes(AreaDeReserva area, LocalDate mes) {
		return null;
	}

	@Override
	public Collection<Reserva> getAll(Condomino condomino) {
		return null;
	}

	@Override
	public Collection<Reserva> doCondominioNoDia(Condominio condominio, LocalDate data) {
		return null;
	}

	@Override
	public Collection<Reserva> pendentesDo(Condominio condominio) {
		return null;
	}
}