package br.com.tagitta.condominio.repository.mock;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.integracao.DAO;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioNoCondominio;
import br.com.tagitta.condominio.repository.CondominiosRepository;

public class CondominiosRepositoryMock implements CondominiosRepository {

	@Override
	public UsuarioNoCondominio getUsuarioNoCondominio(Usuario usuario, Condominio condominio) {
		DAO dao = dao();
		Collection<UsuarioNoCondominio> usuarios = dao.getObjetos(UsuarioNoCondominio.class);
		Optional<UsuarioNoCondominio> usuarioNoCondominio = usuarios.stream()
				.filter(u -> u.getCondominio().equals(condominio) && u.getUsuario().equals(usuario)).findFirst();
		return usuarioNoCondominio.isPresent() ? usuarioNoCondominio.get() : null;
	}

	private DAO dao() {
		return Aplicacao.get().getDAO();
	}

	@Override
	public Collection<Condomino> getCondominos(Condominio condominio) {
		Collection<Condomino> condominos = dao().getObjetos(Condomino.class);
		return condominos.stream().filter(c -> c.getUsuarioNoCondominio().getCondominio().equals(condominio)).collect(Collectors.toList());
	}

}
