package br.com.tagitta.condominio.repository.mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import base.test.util.BaseTest;
import br.com.tagitta.condominio.initializer.ContextInitializer;
import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Condomino;
import br.com.tagitta.condominio.model.Reserva;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.UsuarioCorrente;
import br.com.tagitta.condominio.test.TestFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "file:src/main/resources/spring/spring.xml", initializers = ContextInitializer.class)
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:condominio-test.properties")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class })
public class ReservasRepositoryTest extends BaseTest {

	private Condominio condominio;
	private Usuario usuario;
	private Condomino condomino;
	private LocalDateTime agora;
	private AreaDeReserva areaDeReserva;
	private Usuario usuarioDoSindico;

	@Override
	public void before() {
		super.before();

		condominio = TestFactory.criaCondominio();
		usuario = TestFactory.criaUsuario();

		usuarioDoSindico = TestFactory.criaUsuario();
		condominio.acrescentaSindico(usuarioDoSindico, LocalDate.now().minusDays(30));

		condomino = condominio.acrescentaCondomino(usuario, "1602H", condominio.getBlocos().get(0));

		areaDeReserva = TestFactory.criaAreaPara(condominio);

		agora = LocalDateTime.now();

		registraUsuario(usuario);
	}

	private void registraUsuario(Usuario usuario) {
		UsuarioCorrente.set(usuario);
	}

	@Test
	public void deveObterReservasDaAreaNoDia() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8);
		LocalDateTime dataFinal = dataInicial.withHour(9).withMinute(0).withSecond(0);

		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		registra(reserva1);
		autoriza(reserva1);

		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial.withHour(9).withMinute(1)).comDataFinal(dataFinal.withHour(10))
				.comDescricao("Evento com amigos").paraArea(areaDeReserva).build();
		registra(reserva2);
		autoriza(reserva2);

		Reserva reserva3 = Reserva.cria().para(condomino).comDataInicial(dataInicial.withHour(10).withMinute(1)).comDataFinal(dataFinal.withHour(11))
				.comDescricao("Evento com amigos").paraArea(areaDeReserva).build();
		registra(reserva3);
		recusa(reserva3);

		Collection<Reserva> reservas = Reserva.reservas().daAreaNoDia(areaDeReserva, agora.toLocalDate());
		assertEquals(2, reservas.size());
		assertTrue(reservas.contains(reserva1));
		assertTrue(reservas.contains(reserva2));
	}

	private void recusa(Reserva reserva3) {
		registraUsuario(usuarioDoSindico);
		reserva3.recusa();
	}

	private void registra(Reserva reserva1) {
		registraUsuario(usuario);
		reserva1.registra();
	}

	private void autoriza(Reserva reserva1) {
		registraUsuario(usuarioDoSindico);
		reserva1.autoriza();
	}

}
