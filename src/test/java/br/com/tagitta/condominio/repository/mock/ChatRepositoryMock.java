package br.com.tagitta.condominio.repository.mock;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.syncode.arquitetura.integracao.DAO;
import br.com.tagitta.condominio.model.Chat;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Mensagem;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.repository.ChatRepository;

public class ChatRepositoryMock implements ChatRepository {

	@Override
	public Collection<Chat> all(Condominio condominio, Usuario usuario, LocalDateTime ultimaAtualizacao) {
		Collection<Chat> chats = dao().getObjetos(Chat.class);
		List<Chat> lista = chats.stream().filter(c -> c.getCondominio().equals(condominio) && c.getParticipantes().contains(usuario))
				.collect(Collectors.toList());
		if (ultimaAtualizacao != null) {
			lista = lista.stream().filter(c -> c.getDataHoraDaUltimaMensagem().isAfter(ultimaAtualizacao)).collect(Collectors.toList());
		}
		return lista;
	}

	@Override
	public Chat getChatEntre(Usuario usuario1, Usuario usuario2) {
		Collection<Chat> chats = dao().getObjetos(Chat.class);
		Optional<Chat> chat = chats.stream().filter(c -> c.getParticipantes().contains(usuario1) && c.getParticipantes().contains(usuario2))
				.sorted(Comparator.comparing(Chat::getUltimaMensagemRecebida).reversed()).findFirst();
		if (chat.isPresent()) {
			return chat.get();
		} else {
			return null;
		}
	}

	private DAO dao() {
		return Aplicacao.get().getDAO();
	}

	@Override
	public Collection<Mensagem> getMensagensAPartirDe(Chat chat, LocalDateTime data) {
		return null;
	}
}
