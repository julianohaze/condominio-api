package br.com.tagitta.condominio.controller;

import java.time.LocalDate;

import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import base.test.util.BaseTest;
import br.com.syncode.arquitetura.util.UtilString;
import br.com.tagitta.condominio.exception.AutenticacaoException;
import br.com.tagitta.condominio.initializer.ContextInitializer;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Usuario;
import br.com.tagitta.condominio.model.to.UsuarioAutenticacao;
import br.com.tagitta.condominio.test.TestFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(value = "file:src/main/resources/spring/spring.xml", initializers = ContextInitializer.class)
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:condominio-test.properties")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class })
public class UsuarioControllerTest extends BaseTest {

	@Autowired
	private UsuarioController usuarioController;

	private Usuario usuario;

	private Condominio condominio;

	@Override
	public void before() {
		super.before();

		usuario = TestFactory.criaUsuario();

		condominio = TestFactory.criaCondominio();
		
		condominio.acrescentaSindico(usuario, LocalDate.now());

		condominio.acrescentaCondomino(usuario, "1602H", condominio.getBlocos().get(0));
	}

	@Test
	public void deveAutenticar() {
		UsuarioAutenticacao usuario = new UsuarioAutenticacao(this.usuario.getEmail(), "123456");
		Usuario usuarioAutenticado = usuarioController.autentica(usuario);

		assertNotNull(usuarioAutenticado);

		String hash = usuarioAutenticado.getHash();
		assertNotNull(hash);
		assertFalse(UtilString.isVazio(hash));

		assertNotEmpty(usuarioAutenticado.getCondominios());

		assertEquals(condominio, usuarioAutenticado.getCondominios().get(0));
	}

	@Test(expected = AutenticacaoException.class)
	public void naoDeveAutenticarQuandoDadosForemInvalidos() {
		UsuarioAutenticacao usuario = new UsuarioAutenticacao(this.usuario.getEmail(), "654321");
		usuarioController.autentica(usuario);
	}
}