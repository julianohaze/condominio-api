package br.com.tagitta.condominio.test;

import br.com.tagitta.condominio.model.AreaDeReserva;
import br.com.tagitta.condominio.model.Bloco;
import br.com.tagitta.condominio.model.Condominio;
import br.com.tagitta.condominio.model.Usuario;

public class TestFactory {

	public static Usuario criaUsuario() {
		return criaUsuario("joao@teste.com");
	}

	public static Usuario criaUsuario(String email) {
		Usuario usuario = new Usuario();
		usuario.setNome("Joao");
		usuario.setEmail(email);
		usuario.setSenha("123456");
		usuario.persiste();

		usuario.setSenha(null);

		return usuario;
	}

	public static Bloco criaBlocoPara(Condominio condominio, String bloco) {
		return condominio.acrescentarBloco(bloco);
	}

	public static Condominio criaCondominio() {
		Condominio condominio = new Condominio();
		condominio.setNome("Ilha Bela");
		condominio.setEndereco("Rua ABC");
		condominio.persiste();

		criaBlocoPara(condominio, "A");
		criaBlocoPara(condominio, "B");
		criaBlocoPara(condominio, "C");
		return condominio;
	}

	public static void mockCondominiosRepository() {

	}

	public static AreaDeReserva criaAreaPara(Condominio condominio) {
		AreaDeReserva areaDeReserva = new AreaDeReserva("Nova área", condominio);
		areaDeReserva.persiste();
		return areaDeReserva;
	}

}
