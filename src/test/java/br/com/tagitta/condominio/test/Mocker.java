package br.com.tagitta.condominio.test;

import static org.powermock.api.mockito.PowerMockito.when;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.repository.AgendamentoDeMudancaRepository;
import br.com.tagitta.condominio.repository.ChatRepository;
import br.com.tagitta.condominio.repository.CondominiosRepository;
import br.com.tagitta.condominio.repository.ReservasRepository;
import br.com.tagitta.condominio.repository.mock.AgendamentoDeMudancaRepositoryMock;
import br.com.tagitta.condominio.repository.mock.ChatRepositoryMock;
import br.com.tagitta.condominio.repository.mock.CondominiosRepositoryMock;
import br.com.tagitta.condominio.repository.mock.ReservasRepositoryMock;

public class Mocker {

	public static void mockRepositories() {
		when(Aplicacao.get().getRepositorio(ChatRepository.class)).thenReturn(new ChatRepositoryMock());
		when(Aplicacao.get().getRepositorio(CondominiosRepository.class)).thenReturn(new CondominiosRepositoryMock());
		when(Aplicacao.get().getRepositorio(ReservasRepository.class)).thenReturn(new ReservasRepositoryMock());
		when(Aplicacao.get().getRepositorio(AgendamentoDeMudancaRepository.class)).thenReturn(new AgendamentoDeMudancaRepositoryMock());
	}
}
