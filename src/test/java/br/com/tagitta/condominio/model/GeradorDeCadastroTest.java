package br.com.tagitta.condominio.model;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import base.test.util.BaseTestMock;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.test.Mocker;
import br.com.tagitta.condominio.utils.CriptografiaUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class GeradorDeCadastroTest extends BaseTestMock {

	@Override
	public void before() {
		super.before();

		Mocker.mockRepositories();
	}

	@Test
	public void deveGerarCadastro() {
		CadastroDeCondominio cadastro = new CadastroDeCondominio();
		cadastro.setNome("Ilha Bela");
		cadastro.setEmail("julianohaze@gmail.com");
		cadastro.setEndereco("Rua F-63 Qd 223 Lt 01/34 Setor Faiçalville, Goiânia Go");
		cadastro.setNomeSindico("Juliano Silva");
		cadastro.setSenha("123456");
		cadastro.acrescentaBloco("A");
		cadastro.acrescentaBloco("B");
		cadastro.acrescentaBloco("C");

		GeradorDeCadastro gerador = new GeradorDeCadastro();
		gerador.gera(cadastro);

		Collection<Condominio> condominios = getObjetos(Condominio.class);
		assertEquals(1, condominios.size());

		Condominio condominio = condominios.iterator().next();
		assertEquals("Ilha Bela", condominio.getNome());
		assertEquals("Rua F-63 Qd 223 Lt 01/34 Setor Faiçalville, Goiânia Go", condominio.getEndereco());
		assertEquals("julianohaze@gmail.com", condominio.getSindico().getUsuario().getEmail());
		assertEquals("Juliano Silva", condominio.getSindico().getUsuario().getNome());
		assertEquals(CriptografiaUtils.criptografa("123456"), condominio.getSindico().getUsuario().getSenhaCrypt());
		assertEquals(3, condominio.getBlocos().size());

	}
}
