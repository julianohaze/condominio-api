package br.com.tagitta.condominio.model;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class ComunicadoTest extends CondominioBaseTestMock {

	@Test
	public void deveCriarComunicado() {
		UsuarioCorrente.set(usuarioDoSindico);

		Comunicado comunicado = new Comunicado();
		comunicado.setTexto("ABC 123");
		comunicado.setTitulo("Título");
		comunicado.setCondominio(condominio);
		comunicado.persiste();

		comunicado = get(Comunicado.class, comunicado.getId());

		assertEquals("ABC 123", comunicado.getTexto());
		assertEquals("Título", comunicado.getTitulo());
		assertEquals(condominio, comunicado.getCondominio());
		assertEquals(LocalDate.now(), comunicado.getData().toLocalDate());
		assertEquals(sindico, comunicado.getSindico());

		UsuarioCorrente.clear();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDeveInserirComunicadoSemTexto() {
		UsuarioCorrente.set(usuarioDoSindico);

		Comunicado comunicado = new Comunicado();
		comunicado.setTitulo("Título");
		comunicado.setCondominio(condominio);
		comunicado.persiste();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDeveInserirComunicadoSemTitulo() {
		UsuarioCorrente.set(usuarioDoSindico);

		Comunicado comunicado = new Comunicado();
		comunicado.setTexto("ABC 123");
		comunicado.setCondominio(condominio);
		comunicado.persiste();
	}

	@Test(expected = UsuarioNaoESindicoException.class)
	public void somenteOSindicoPodeCriarComunicados() {
		UsuarioCorrente.set(usuario);

		Comunicado comunicado = new Comunicado();
		comunicado.setTexto("ABC 123");
		comunicado.setTitulo("Título");
		comunicado.setCondominio(condominio);
		comunicado.persiste();
	}
}
