package br.com.tagitta.condominio.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import base.test.util.BaseTestMock;
import br.com.tagitta.condominio.test.Mocker;
import br.com.tagitta.condominio.test.TestFactory;

public abstract class CondominioBaseTestMock extends BaseTestMock {

	protected Condominio condominio;
	protected Usuario usuario;
	protected Condomino condomino;
	protected LocalDateTime agora;
	protected AreaDeReserva areaDeReserva;
	protected Usuario usuarioDoSindico;
	protected Usuario autorizador;
	protected Sindico sindico;

	@Override
	public void before() {
		super.before();

		Mocker.mockRepositories();

		condominio = TestFactory.criaCondominio();
		usuario = TestFactory.criaUsuario();

		usuarioDoSindico = TestFactory.criaUsuario();
		sindico = condominio.acrescentaSindico(usuarioDoSindico, LocalDate.now().minusDays(30));

		condomino = condominio.acrescentaCondomino(usuario, "1602H", condominio.getBlocos().get(0));

		areaDeReserva = TestFactory.criaAreaPara(condominio);

		agora = LocalDateTime.now();

		autorizador = condomino.getCondominio().getSindico().getUsuario();
	}
}
