package br.com.tagitta.condominio.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import base.test.util.BaseTestMock;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.model.tipo.EstadoDaMensagem;
import br.com.tagitta.condominio.test.TestFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class ChatTest extends BaseTestMock {

	private Usuario usuario1;
	private Usuario usuario2;
	private Condominio condominio;

	@Override
	public void before() {
		super.before();

		condominio = TestFactory.criaCondominio();

		usuario1 = TestFactory.criaUsuario("usuario1@gmail.com");
		usuario2 = TestFactory.criaUsuario("usuario2@gmail.com");
	}

	@Test
	public void deveCriarChatEntreDoisUsuarios() {
		Chat chat = Chat.cria(condominio, usuario1, usuario2);

		assertNotNull(chat.getInicio());
		assertEquals(LocalDate.now(), chat.getInicio().toLocalDate());
		assertNull(chat.getTermino());

		assertThatChatContains(chat, usuario1);
	}

	@Test
	public void deveIncluirMensagem() {
		Chat chat = Chat.cria(condominio, usuario1, usuario2);

		LocalDateTime dataHora = LocalDateTime.now();
		Mensagem mensagem = Mensagem.cria().para(chat).doUsuario(usuario1).comTexto("Teste").naData(dataHora).build();
		mensagem.inclui();

		mensagem = get(Mensagem.class, mensagem.getId());
		assertEquals(usuario1, mensagem.getUsuario());
		assertEquals("Teste", mensagem.getTexto());
		assertEquals(dataHora, mensagem.getDataHora());
		assertEquals(mensagem, chat.getUltimaMensagemRecebida());
		assertEquals(mensagem.getDataHora(), chat.getDataHoraDaUltimaMensagem());
		assertEquals(EstadoDaMensagem.ENVIADA, mensagem.getEstado());
	}

	@Test
	public void deveEntregarMensagem() {
		Chat chat = Chat.cria(condominio, usuario1, usuario2);

		LocalDateTime dataHora = LocalDateTime.now();
		Mensagem mensagem = Mensagem.cria().para(chat).doUsuario(usuario1).comTexto("Teste").naData(dataHora).build();
		mensagem.persiste();

		mensagem.confirmaRecebimento();

		assertEquals(EstadoDaMensagem.ENTREGUE, mensagem.getEstado());
	}

	@Test
	public void deveLerMensagem() {
		Chat chat = Chat.cria(condominio, usuario1, usuario2);
		
		LocalDateTime dataHora = LocalDateTime.now();
		Mensagem mensagem = Mensagem.cria().para(chat).doUsuario(usuario1).comTexto("Teste").naData(dataHora).build();
		mensagem.persiste();
		
		mensagem.confirmaLeitura();
		
		assertEquals(EstadoDaMensagem.LIDA, mensagem.getEstado());
	}

	private void assertThatChatContains(Chat chat, Usuario usuario) {
		boolean achou = false;
		for (Usuario participante : chat.getParticipantes()) {
			if (participante.equals(usuario)) {
				achou = true;
				break;
			}
		}
		if (!achou) {
			fail("Participante " + usuario.getEmail() + " não foi encontrado no chat.");
		}
	}
}
