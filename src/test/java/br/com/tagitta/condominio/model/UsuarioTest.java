package br.com.tagitta.condominio.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import base.test.util.BaseTestMock;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.test.TestFactory;
import br.com.tagitta.condominio.utils.CriptografiaUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class UsuarioTest extends BaseTestMock {

	private Usuario usuario;

	@Override
	public void before() {
		super.before();

		usuario = TestFactory.criaUsuario();
	}

	@Test
	public void deveCriptografarSenha() {
		usuario = get(Usuario.class, usuario.getId());
		assertEquals(CriptografiaUtils.criptografa("123456"), usuario.getSenhaCrypt());
	}

	@Test
	public void naoDeveAtualizarASenhaQuandoAtualizarOUsuario() {
		usuario.setSenha("654321");
		usuario.persiste();

		usuario = get(Usuario.class, usuario.getId());
		assertEquals(CriptografiaUtils.criptografa("123456"), usuario.getSenhaCrypt());
	}

	@Test
	public void deveAtualizarSenha() {
		usuario.atualizaSenha("654321");
		usuario = get(Usuario.class, usuario.getId());
		assertEquals(CriptografiaUtils.criptografa("654321"), usuario.getSenhaCrypt());
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDevePersistirUsuarioComEmailNulo() {
		usuario.setEmail(null);
		usuario.persiste();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDevePersistirUsuarioComEmailVazio() {
		usuario.setEmail(" ");
		usuario.persiste();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDevePersistirUsuarioComNomeNulo() {
		usuario.setNome(null);
		usuario.persiste();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDevePersistirUsuarioComNomeVazio() {
		usuario.setNome(" ");
		usuario.persiste();
	}

	@Test()
	public void naoDevePersistirEmailInvalido() {
		try {
			usuario.setEmail("abc");
			usuario.persiste();
			fail("Permitiu e-mail sem @ e sem ponto");
		} catch (DadosInvalidosException e) {
		}

		try {
			usuario.setEmail("abc@teste");
			usuario.persiste();
			fail("Permitiu e-mail sem @ e sem ponto");
		} catch (DadosInvalidosException e) {
		}
	}

}
