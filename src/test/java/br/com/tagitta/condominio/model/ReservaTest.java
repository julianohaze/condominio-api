package br.com.tagitta.condominio.model;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.ReservaComDatasConflitantesException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;
import br.com.tagitta.condominio.model.tipo.EstadoDaReserva;
import br.com.tagitta.condominio.model.tipo.TipoDeEvento;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class ReservaTest extends CondominioBaseTestMock {

	@Test
	public void deveRegistrarReserva() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva.registra();

		reserva = get(Reserva.class, reserva.getId());

		assertEquals(dataInicial, reserva.getDataInicial());
		assertEquals(dataFinal, reserva.getDataFinal());

		assertEquals(condominio, reserva.getCondomino().getUsuarioNoCondominio().getCondominio());
		assertEquals(agora.toLocalDate(), reserva.getDataSolicitacao().toLocalDate());

		assertNull(reserva.getDataAutorizacao());
		assertNull(reserva.getDataRecusa());

		assertEquals(EstadoDaReserva.PENDENTE, reserva.getEstado());
	}

	@Test
	public void deveGerarEventoAoRegistrarReserva() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva.registra();

		reserva = get(Reserva.class, reserva.getId());

		Evento evento = reserva.getEvento();
		assertNotNull(evento);
		assertEquals(reserva.getDescricao(), evento.getDescricao());
		assertEquals(reserva.getDataInicial(), evento.getData());
		assertEquals(TipoDeEvento.RESERVA, evento.getTipo());
		assertEquals(condominio, evento.getCondominio());
	}

	@Test
	public void deveRegistrarReservaParaODiaTodo() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);

		Reserva reserva = Reserva.cria().para(condomino).paraDiaTodo(true).comDataInicial(dataInicial).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva.registra();

		reserva = get(Reserva.class, reserva.getId());

		assertEquals(dataInicial.withHour(0).withMinute(0).withSecond(0), reserva.getDataInicial());
		assertEquals(dataInicial.withHour(23).withMinute(59).withSecond(59), reserva.getDataFinal());
		assertTrue(reserva.isDiaTodo());
	}

	@Test
	public void deveRegistrarReservaComHorarioSequenciais() {
		LocalDateTime dataInicial = agora.withHour(8).withMinute(0).withSecond(0).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(9).withMinute(0).withSecond(0);

		// mesmo horário
		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva1.registra();

		dataInicial = dataInicial.withHour(9).withSecond(1);
		dataFinal = dataFinal.withHour(10);

		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva2.registra();
	}

	@Test
	public void deveRegistrarDuasReservasComHorariosDiferentes() {
		LocalDateTime dataInicial = agora.withHour(8).withMinute(0).withSecond(0).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(12).withMinute(0).withSecond(0);

		// mesmo horário
		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva1.registra();

		dataInicial = dataInicial.withHour(13);
		dataFinal = dataInicial.withHour(18);

		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva2.registra();
	}

	@Test(expected = ReservaComDatasConflitantesException.class)
	public void naoDeveRegistrarReservaComMesmoHorario() {
		LocalDateTime dataInicial = agora.withHour(8).withMinute(0).withSecond(0).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		// mesmo horário
		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva1.registra();
		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva2.registra();
	}

	@Test(expected = ReservaComDatasConflitantesException.class)
	public void naoDeveRegistrarReservaComHorarioDentroDoPeriodo() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		// mesmo horário
		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva1.registra();

		dataInicial = dataInicial.withHour(13);

		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva2.registra();
	}

	@Test(expected = ReservaComDatasConflitantesException.class)
	public void naoDeveRegistrarReservaComPeriodosColidindo() {
		LocalDateTime dataInicial = agora.withHour(8).withMinute(0).withSecond(0).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(12).withMinute(0).withSecond(0);

		// mesmo horário
		Reserva reserva1 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva1.registra();

		dataInicial = dataInicial.withHour(11);
		dataFinal = dataInicial.withHour(15);

		Reserva reserva2 = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();
		reserva2.registra();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDeveRegistrarReservaSemInformarArea() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos").build();
		reserva.registra();
	}

	@Test(expected = DadosInvalidosException.class)
	public void naoDeveRegistrarReservaSemInformarDataInicial() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).paraArea(areaDeReserva).comDataFinal(dataFinal).comDescricao("Evento com amigos").build();
		reserva.registra();
	}

	@Test(expected = DadosInvalidosException.class)
	public void deveInformarInformarDataFinalSeReservaNaoForParaODiaTodo() {
		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);

		Reserva reserva = Reserva.cria().para(condomino).paraArea(areaDeReserva).comDataInicial(dataInicial).comDescricao("Evento com amigos").build();
		reserva.registra();
	}

	@Test(expected = UsuarioNaoESindicoException.class)
	public void somenteOSindicoPodeAutorizarUmaReserva() {
		UsuarioCorrente.set(usuario);

		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();

		reserva.registra();
		reserva.autoriza();
	}

	@Test
	public void deveAutorizarReserva() {
		UsuarioCorrente.set(autorizador);

		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();

		reserva.registra();
		reserva.autoriza();

		reserva = get(Reserva.class, reserva.getId());

		assertEquals(autorizador, reserva.getAutorizador());
		assertEquals(EstadoDaReserva.AUTORIZADA, reserva.getEstado());
		assertNotNull(reserva.getDataAutorizacao());
		assertNull(reserva.getDataRecusa());
		assertEquals(agora.toLocalDate(), reserva.getDataAutorizacao().toLocalDate());

		UsuarioCorrente.clear();
	}

	@Test(expected = UsuarioNaoESindicoException.class)
	public void somenteOSindicoPodeRecusarUmaReserva() {
		UsuarioCorrente.set(usuario);

		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();

		reserva.registra();
		reserva.recusa();
	}

	@Test
	public void deveRecusarReserva() {
		UsuarioCorrente.set(autorizador);

		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();

		reserva.registra();
		reserva.recusa();

		reserva = get(Reserva.class, reserva.getId());

		assertEquals(autorizador, reserva.getAutorizador());
		assertEquals(EstadoDaReserva.RECUSADA, reserva.getEstado());
		assertNull(reserva.getDataAutorizacao());
		assertNotNull(reserva.getDataRecusa());
		assertEquals(agora.toLocalDate(), reserva.getDataRecusa().toLocalDate());

		UsuarioCorrente.clear();
	}

	@Test
	public void deveCancelarReserva() {
		UsuarioCorrente.set(autorizador);

		LocalDateTime dataInicial = agora.withMinute(0).withSecond(0).withHour(8).plusDays(5);
		LocalDateTime dataFinal = dataInicial.withHour(18).withMinute(0).withSecond(0);

		Reserva reserva = Reserva.cria().para(condomino).comDataInicial(dataInicial).comDataFinal(dataFinal).comDescricao("Evento com amigos")
				.paraArea(areaDeReserva).build();

		reserva.registra();
		reserva.cancela();

		reserva = get(Reserva.class, reserva.getId());

		assertEquals(EstadoDaReserva.CANCELADA, reserva.getEstado());
		assertNotNull(reserva.getDataCancelamento());
		assertEquals(agora.toLocalDate(), reserva.getDataCancelamento().toLocalDate());

		UsuarioCorrente.clear();
	}
}
