package br.com.tagitta.condominio.model;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import base.test.util.BaseTestMock;
import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.exception.UsuarioJaPossuiPapelNoCondominioException;
import br.com.tagitta.condominio.model.tipo.TipoDePapelNoCondominio;
import br.com.tagitta.condominio.test.Mocker;
import br.com.tagitta.condominio.test.TestFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Aplicacao.class})
public class CondominioTest extends BaseTestMock {

  private Usuario usuario;
  private Condominio condominio;
  private Usuario usuarioSindico;

  @Override
  public void before() {
    super.before();

    Mocker.mockRepositories();

    usuario = TestFactory.criaUsuario();

    condominio = TestFactory.criaCondominio();

    usuarioSindico = TestFactory.criaUsuario();
    condominio.acrescentaSindico(usuarioSindico, LocalDate.now());
  }

  @Test
  public void deveAcrescentarUsuarioComoCondomino() {
    Bloco blocoA = condominio.getBlocos().get(0);

    condominio.acrescentaCondomino(usuario, "1602H", blocoA);

    usuario = get(Usuario.class, usuario.getId());
    assertTrue(usuario.isCondominoDo(condominio));

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio();
    assertNotNull(usuarioNoCondominio);
    assertNotEmpty(usuarioNoCondominio.getPapeis());

    PapelNoCondominio papelNoCondominio = usuarioNoCondominio.getPapeis().iterator().next();
    assertEquals(TipoDePapelNoCondominio.CONDOMINO, papelNoCondominio.getTipo());
    assertEquals("1602H", ((Condomino) (papelNoCondominio)).getUnidade());
    assertEquals(blocoA, ((Condomino) (papelNoCondominio)).getBloco());
  }

  @Test
  public void deveCriarChatComSindicoQuandoCriarCondomino() {
    Bloco blocoA = condominio.getBlocos().get(0);

    Condomino condomino = condominio.acrescentaCondomino(usuario, "1602H", blocoA);

    Usuario usuario = condomino.getUsuario();
    Chat chat = usuario.getChatCom(usuarioSindico);
    assertNotNull(chat);
    assertNull(chat.getTermino());
  }

  @Test
  public void deveAcrescentarUsuarioComoSindico() {
    LocalDate agora = LocalDate.now();
    condominio.acrescentaSindico(usuario, agora);

    usuario = get(Usuario.class, usuario.getId());
    assertTrue(usuario.isSindicoDo(condominio));

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio();
    assertNotNull(usuarioNoCondominio);
    assertNotEmpty(usuarioNoCondominio.getPapeis());

    PapelNoCondominio papelNoCondominio = usuarioNoCondominio.getPapeis().iterator().next();
    assertEquals(TipoDePapelNoCondominio.SINDICO, papelNoCondominio.getTipo());

    Sindico sindico = (Sindico) (papelNoCondominio);
    assertEquals(agora, sindico.getEntrada());
    assertNull(sindico.getSaida());

    assertEquals(condominio.getSindico(), sindico);
  }

  @Test
  public void deveCriarChatComTodosCondominosQuandoCriarSindico() {
    Usuario usuario1 = TestFactory.criaUsuario();
    condominio.acrescentaCondomino(usuario1, "1");

    Usuario usuario2 = TestFactory.criaUsuario();
    condominio.acrescentaCondomino(usuario2, "2");

    LocalDate agora = LocalDate.now();
    condominio.acrescentaSindico(usuario, agora);

    Chat chat1 = usuario.getChatCom(usuario1);
    assertNotNull(chat1);

    Chat chat2 = usuario.getChatCom(usuario2);
    assertNotNull(chat2);
  }

  @Test
  public void deveAcrescentarUsuarioComoAdministrador() {
    condominio.acrescentaAdministrador(usuario);

    usuario = get(Usuario.class, usuario.getId());
    assertTrue(usuario.isAdministradorDo(condominio));

    UsuarioNoCondominio usuarioNoCondominio = getUsuarioNoCondominio();
    assertNotNull(usuarioNoCondominio);
    assertNotEmpty(usuarioNoCondominio.getPapeis());

    PapelNoCondominio papelNoCondominio = usuarioNoCondominio.getPapeis().iterator().next();
    assertEquals(TipoDePapelNoCondominio.ADMINISTRADOR, papelNoCondominio.getTipo());
  }

  @Test(expected = UsuarioJaPossuiPapelNoCondominioException.class)
  public void naoDeveAcrescentarUsuarioComoCondomino2Vezes() {
    Bloco blocoA = condominio.getBlocos().get(0);
    condominio.acrescentaCondomino(usuario, "1602H", blocoA);
    condominio.acrescentaCondomino(usuario, "1602H", blocoA);
  }

  @Test(expected = UsuarioJaPossuiPapelNoCondominioException.class)
  public void naoDeveAcrescentarUsuarioComoAdministradorVezes() {
    condominio.acrescentaAdministrador(usuario);
    condominio.acrescentaAdministrador(usuario);
  }

  @Test(expected = UsuarioJaPossuiPapelNoCondominioException.class)
  public void naoDeveAcrescentarUsuarioComoSindicoVezes() {
    condominio.acrescentaSindico(usuario, LocalDate.now());
    condominio.acrescentaSindico(usuario, LocalDate.now());
  }

  @Test
  public void deveAcrescentarBlocoNoCondominio() {
    Bloco blocoA = new Bloco("Bloco A");
    condominio.acrescentarBloco(blocoA);

    Collection<Bloco> blocos = getObjetos(Bloco.class);
    assertTrue(blocos.stream().filter(b -> b.getDescricao().equals("Bloco A")).findAny().isPresent());
  }

  @Test(expected = IllegalArgumentException.class)
  public void naoDeveAcrescentarBlocoComDescricaoVazia() {
    Bloco blocoA = new Bloco("");
    condominio.acrescentarBloco(blocoA);
  }

  private UsuarioNoCondominio getUsuarioNoCondominio() {
    return Condominio.repository().getUsuarioNoCondominio(usuario, condominio);
  }
}
