package br.com.tagitta.condominio.model;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.syncode.arquitetura.aplicacao.Aplicacao;
import br.com.tagitta.condominio.exception.AgendamentoDeMudancaException;
import br.com.tagitta.condominio.exception.DadosInvalidosException;
import br.com.tagitta.condominio.exception.UsuarioNaoESindicoException;
import br.com.tagitta.condominio.model.tipo.EstadoDoAgendamentoDeMudanca;
import br.com.tagitta.condominio.model.tipo.Periodo;
import br.com.tagitta.condominio.model.tipo.TipoDeAgendamentoDeMudanca;
import br.com.tagitta.condominio.test.TestFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Aplicacao.class })
public class AgendamentoDeMudancaTest extends CondominioBaseTestMock {

	private Condomino condomino2;

	@Override
	public void before() {
		super.before();

		Usuario usuario2 = TestFactory.criaUsuario();
		condomino2 = condominio.acrescentaCondomino(usuario2, "1601H", condominio.getBlocos().get(0));
	}

	@Test
	public void deveSolicitarAgendamentoDeMudanca() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();

		assertEquals(agendamento.getEstado(), EstadoDoAgendamentoDeMudanca.PENDENTE);
		assertEquals(agendamento.getCondomino(), condomino);
		assertEquals(agendamento.getData(), LocalDate.now());
		assertEquals(agendamento.getDataSolicitacao().toLocalDate(), LocalDate.now());
		assertEquals(agendamento.getTipo(), TipoDeAgendamentoDeMudanca.ENTRAR);
		assertNull(agendamento.getAutorizador());
		assertNull(agendamento.getDataAutorizacao());
		assertNull(agendamento.getDataRecusa());
		assertNotNull(agendamento.getEvento());
	}

	@Test(expected = DadosInvalidosException.class)
	public void deveValidarTipoDoAgendamento() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();
	}

	@Test(expected = DadosInvalidosException.class)
	public void deveValidarDataDoAgendamento() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR).para(condomino)
				.build();
		agendamento.solicitar();
	}

	@Test(expected = DadosInvalidosException.class)
	public void deveValidarPeriodoDoAgendamento() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().doTipo(TipoDeAgendamentoDeMudanca.ENTRAR).naData(LocalDate.now()).para(condomino)
				.build();
		agendamento.solicitar();
	}

	@Test
	public void deveAutorizarAgendamentoDeMudanca() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();

		UsuarioCorrente.set(autorizador);

		agendamento.autorizar();

		assertEquals(agendamento.getEstado(), EstadoDoAgendamentoDeMudanca.AUTORIZADA);
		assertEquals(LocalDate.now(), agendamento.getDataAutorizacao().toLocalDate());
		assertEquals(sindico.getUsuario(), agendamento.getAutorizador());
		assertNull(agendamento.getDataRecusa());

		UsuarioCorrente.clear();
	}

	@Test
	public void deveRecusarAgendamentoDeMudanca() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();

		UsuarioCorrente.set(autorizador);

		agendamento.recusar();

		assertEquals(agendamento.getEstado(), EstadoDoAgendamentoDeMudanca.RECUSADA);
		assertEquals(LocalDate.now(), agendamento.getDataRecusa().toLocalDate());
		assertEquals(sindico.getUsuario(), agendamento.getAutorizador());
		assertNull(agendamento.getDataAutorizacao());

		UsuarioCorrente.clear();
	}

	@Test(expected = UsuarioNaoESindicoException.class)
	public void somenteSindicoPodeAutorizarAgendamento() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();

		UsuarioCorrente.set(condomino.getUsuarioNoCondominio().getUsuario());
		agendamento.autorizar();
	}


	@Test(expected = AgendamentoDeMudancaException.class)
	public void soDevePermitirUmAgendamentoPorDiaEPeriodo() {
		AgendamentoDeMudanca agendamento1 = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento1.solicitar();

		AgendamentoDeMudanca agendamento2 = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino2).build();
		agendamento2.solicitar();
	}
	
	@Test
	public void deveCancelarAgendamentoDeMudanca() {
		AgendamentoDeMudanca agendamento = AgendamentoDeMudanca.cria().noPeriodo(Periodo.MANHA).doTipo(TipoDeAgendamentoDeMudanca.ENTRAR)
				.naData(LocalDate.now()).para(condomino).build();
		agendamento.solicitar();		
		
		agendamento.cancelar();
		
		agendamento = get(AgendamentoDeMudanca.class, agendamento.getId());
		
		assertEquals(EstadoDoAgendamentoDeMudanca.CANCELADA, agendamento.getEstado());
		assertNotNull(agendamento.getDataCancelamento());
		assertEquals(agora.toLocalDate(), agendamento.getDataCancelamento().toLocalDate());
	}
}
